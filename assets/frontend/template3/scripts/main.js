$(function(){
    
    var rightMenuToggle      = $("header .menu-toggle");
    var navUl                = $("header nav ul");
    var header               = $("header");
    var rightMenu            = $(".right-menu");
    var rightMenuClose       =  $(".right-menu-close");
    /* menu open/close */
    rightMenuToggle.click((e) =>{
        $('.bg-fade')
            .addClass('bg-fade-on');
        $('html, body').css("overflow", "hidden");
        rightMenu.addClass("right-menu-open");
    });

    rightMenuClose.click((e) =>{
        e.preventDefault();
        $('.bg-fade').removeClass('bg-fade-on');
        $('html, body').css("overflow", "auto");
       rightMenu.removeClass("right-menu-open");
    });

    $('*').click((e) =>{
    
        if ( !$(e.target).is('.right-menu') && !$(e.target).is('.right-menu *') && !$(e.target).is('header *')){
            $('.bg-fade').removeClass('bg-fade-on');
            $('html, body').css("overflow", "auto");
            rightMenu.removeClass("right-menu-open");
        }
    });


    if ($(window).width() < 768) {

    }else{}

});

