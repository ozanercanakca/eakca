$(function(){
    
    var menuToggle    = $("header .menu-toggle");
    var navUl         = $("header nav ul");
    var header        = $("header");
    var searchBtn     = $("header .search-btn");
    var searchContent = $(".search-content")

    /* menu open/close */
    menuToggle.click("on", function(){
        navUl.slideToggle(300);
        $(this).toggleClass("menu-toggle-closed");
    });

    if ($(window).width() < 768) {
        $('*').click(function(e){
            if ( !$(e.target).is('header') && !$(e.target).is('header *') ){
                navUl.hide(300);
                menuToggle.removeClass("menu-toggle-closed");
            }
        });

        searchBtn.click(function(){
            navUl.hide(300);
            menuToggle.removeClass("menu-toggle-closed");
        });

    }

    searchBtn.click("on", function(e){
        e.preventDefault();
        searchContent.addClass("search-content-open");
    });

    $(".search-content-closed").click(function(){
        searchContent.removeClass("search-content-open");        
    });

});

$(document).ready(function () {});