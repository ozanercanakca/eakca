$(function(){

    $('.dashboard-menu .menu-toggle').click(function(){
        $('.dashboard-menu  ul').slideToggle(200);
    });

    $('.user-menu a').click(function(e){
        // e.preventDefault();
        $('.user-menu .dropdown').slideToggle(200);
    });

    $('.security-page .active').click(function(e){
        e.preventDefault();
        $('.hide-content').removeClass('d-none');
        $('.security-page .active').hide();
        $('.security-page .passive').show();
    });
    $('.security-page .passive').click(function(e){
        e.preventDefault();
        $('.hide-content').addClass('d-none');
        $('.security-page .passive').hide();
        $('.security-page .active').show();
    });

    // tab menu
    $("ul.tab-menu li:first").addClass("active");
    $("div.tab-content").hide();
    $("div.tab-content:first").show();

    $("ul.tab-menu li").click(function(e){
        var index = $(this).index();
        $("ul.tab-menu li").removeClass("active");
        $(this).addClass("active");
        $("div.tab-content").slideUp(300);
        $("div.tab-content:eq("+ index +")").slideDown(300);
        return false;
    });

     /* WOW animate */
     wow = new WOW(
       {
           boxClass:     'wow',      // default<
           animateClass: 'animated', // default
           offset:       0,          // default
           mobile:       true,       // default
           live:         true        // default
       }
   )
   wow.init();
});