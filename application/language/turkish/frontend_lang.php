<?php
/*btcoinline.com - turkish lang param*/
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['test'] = "Turkish  EA";
$lang['m1'] = "Yardım";
$lang['m2'] = "Oturum Aç";
$lang['m3'] = "Kayıt Ol";
$lang['m4'] = "Panel";
$lang['c1'] = '
                  <div class="col-md-10">
                      <div>
                          <h4>MOBILE WALLET</h4>
                          <p>Our popular wallet works on your Android or iPhone in addition to your web browser.</p>
                          <a href="" class="read-more">Read More</a>
                      </div>
                  </div>
                  ';
$lang['c2'] = '
                  <div class="col-md-10">
                      <div>
                          <h4>Secure storage</h4>
                          <p>Our popular wallet works on your Android or iPhone in addition to your web browser.</p>
                          <a href="" class="read-more">Read More</a>
                      </div>
                  </div>
                  ';
$lang['c3'] = '
                  <div class="col-md-10">
                      <div>
                          <h4>Insurance ProtectIon</h4>
                          <p>Our popular wallet works on your Android or iPhone in addition to your web browser.</p>
                          <a href="" class="read-more">Read More</a>
                      </div>
                  </div>
                  ';
$lang['c4'] = '
                  <div class="col-md-10">
                      <div>
                          <h4>Full Control</h4>
                          <p>Our popular wallet works on your Android or iPhone in addition to your web browser.</p>
                          <a href="" class="read-more">Read More</a>
                      </div>
                  </div>
                  ';

$lang['f1'] = "Dil";

/*DASHBOARD*/
$lang['dm1'] = "Panel";
$lang['dm2'] = "BTC Yatır/Çek";
$lang['dm3'] = "TRY Yatır/Çek";
$lang['dm4'] = "Banka Hesapları";
$lang['dm5'] = "Ayarlar";
$lang['dm6'] = "Güvenlik";
$lang['dm7'] = "Hesap";
$lang['dm8'] = "Parola";
$lang['dm9'] = "Hesap Hareketleri";
$lang['dm10'] = "Oturumu Kapat";


/*PARTNER*/
$lang['p1'] = "Oturum Aç";
$lang['p2'] = "E-Posta";
$lang['p3'] = "Parolamı Unuttum";
$lang['p4'] = "Hesabınız yok mu? Kayıt Ol";
$lang['p5'] = "Google Auth Kod";
$lang['p6'] = "Oturum Aç";
$lang['p7'] = "Kayıt Olun";
$lang['p8'] = "Parola (Tekrar)";
$lang['p9'] = "Kayıt";
$lang['p10'] = "Aktivasyon Doğrula";
$lang['p11'] = "Kayıp Parola Doğrula ";
$lang['p12'] = "Parola Sıfırla";
$lang['p13'] = "Parola ";

/*Auth Request Alerts*/
$lang['a1'] = "Yanlış e-Posta / parola";
$lang['a2'] = "Lütfen robot olmadığınızı onaylayın!";
$lang['a3'] = "Başarılı! Yönlendiriliyorsunuz...";
$lang['a4'] = "Başarısız Google 2FA kod! ";
$lang['a5'] = "Parola Güncelleme Hakkında";
$lang['a6'] = "Hesap Aktivasyonu Hakkında";
$lang['a7'] = "Hesabınız oluşturuldu.";
$lang['a8'] = "Parolamı Unuttum";
$lang['a9'] = "Parolamı Unuttum";
$lang['a10'] = "Parolanızı değiştirmek için tıklayın.";
$lang['a11'] = "Parola Sıfırlama İşlemi";
$lang['a12'] = "Parola Sıfırlama Onay";
$lang['a13'] = "Hesap Onaylama";
$lang['a14'] = "";
$lang['a15'] = "";
$lang['mail1'] = "Hesabınız oluşturuldu";
$lang['mail2'] = "Kayıt işleminizi etkinleştirmek için aşağıdaki etkinleştirme bağlantısını tıklayın.";
$lang['mail3'] = "Tıkla";
$lang['mail4'] = "Kaydınız başarılı. E-posta kutunuzu kontrol etmeniz ve etkinleştirme işlemini tamamlamanız gerekir. Gelen kutunuzda görmüyorsanız, spam kutunuzu kontrol edin.";
$lang['mail5'] = "E-posta gönderilemedi. Lütfen iletişim kurun.";
$lang['mail6'] = "Daha sonra tekrar deneyin!";
$lang['mail7'] = "Bu e-posta adresi sisteme kayıtlıdır.";
$lang['mail8'] = "Şifreler eşleşmiyor!";
$lang['mail9'] = "Parola yenilemesi için talimatlar e-posta adresinize gönderildi. Gelen kutunuzda görmüyorsanız spam kutunuzu kontrol edin.";
$lang['mail10'] = "Daha sonra tekrar deneyin!";
$lang['mail11'] = "Daha sonra tekrar deneyin!";
$lang['mail12'] = "Sistemde kayıtlı değilsiniz!";
$lang['mail13'] = "Tarih geçtiği için hesabınız onaylanmadı.";
$lang['mail14'] = "Onaylandı. Parola değiştirme sayfasına yönlendiriliyorsunuz.";
$lang['mail15'] = "Başarısız. Tekrar deneyiniz!";
$lang['mail16'] = "Şifreyi Unuttum Hakkında";
$lang['mail17'] = 'Şifreniz güncellenmiştir.';
$lang['mail18'] = "Parola sıfırlama işlemi başarısız oldu.";
$lang['mail19'] = "Bu e-posta adresi kullanılmıyor!";
$lang['mail20'] = "Şifreler eşleşmiyor!";
$lang['mail21'] = "e-Posta hesabınız onaylandı.";
$lang['mail22'] = "Etkinleştirme işlemi yapılamadı. Tekrar deneyin!";
$lang['mail23'] = "Şifreniz güncellenmiştir.";
$lang['mail24'] = "";
$lang['mail25'] = "";

/*Account Request Controller*/
$lang["ar1"] = "Ad";
$lang["ar2"] = "Soyad";
$lang["ar3"] = "Telefon";
$lang["ar4"] = "Ülke";
$lang["ar5"] = "Şehir";
$lang["ar6"] = "İlçe";
$lang["ar7"] = "Profil Güncelleme";
$lang["ar8"] = "Güncelleme başarılı!";
$lang["ar9"] = "Güncelleme başarısız!";
$lang["ar10"] = "Tamam";
$lang["ar11"] = "<strong>Uyarı!</strong> Profilinizi tamamladığınızda, hesabınız Advanced durumuna geçecektir. BTC ve TRY işlemlerindeki sınırlarınız güncellenecek.";
$lang["ar12"] = "<strong>Bilgi!</strong> Advanced üyeliğiniz mevcut.";
$lang["ar13"] = "Mevcut Parola";
$lang["ar14"] = "Yeni Parola";
$lang["ar15"] = "Yeni Parola (Tekrar)";
$lang["ar16"] = "Parola Güncelleme";
$lang["ar17"] = "Parolalar uyuşmuyor!";
$lang["ar18"] = "Mevcut parolanız yanlış!";
$lang["ar19"] = "6 Haneli Kod";
$lang["ar20"] = "Kod eşleşmedi!";
$lang["ar21"] = "Kod eşleşti!";
$lang["ar22"] = "<strong>Bilgi!</strong> Private üyeliğiniz mevcut.";
$lang["ar23"] = "Google Authenticator uygulamasını yükleyiniz.";
$lang["ar24"] = "QR kodu Google Authenticator aracılığıyla taradıktan sonra üretilen kodu aşağıdaki alana giriniz.";
$lang["ar25"] = '<div class="alert alert-success">
                      <strong>Durum!</strong> Google Two-Factor Authentication aktifleştirilmiş.
                     </div>';
$lang["ar26"] = '<div class="alert alert-warning">
                      <strong>Durum!</strong> Google Two-Factor Authentication aktifleştirilmemiş.
                     </div>';

$lang["ar27"] = "e-Posta Aktivasyon ";
$lang["ar28"] = "Google 2 Adımlı Oturum";
$lang["ar29"] = '<div class="alert alert-success">
                      <strong>Durum!</strong> E-Posta aktivasyonunuzu gerçekleştirmişsiniz.
                    </div>';
$lang["ar30"] = '<div class="alert alert-warning">
                          <strong>Uyarı!</strong> E-Posta aktivasyonunuzu gerçekleştirmemişsiniz.
                      </div>
                      <button type="button" id="securityUp2" class="btn btn-primary btn-block">Etkinleştir</button>';
$lang["ar31"] = 'Kayıt işleminizi etkinleştirmek için aşağıdaki etkinleştirme bağlantısını tıklayın:';
$lang["ar32"] = 'Aktivasyon maili e-posta adresinize gönderildi.';
$lang["ar33"] = 'Daha sonra tekrar deneyiniz';
$lang["ar34"] = 'Bildirim Tipi';
$lang["ar35"] = 'Değer';
$lang["ar36"] = 'Miktar';
$lang["ar37"] = 'Banka';
$lang["ar38"] = 'İşlem Tipi';
$lang["ar39"] = 'Talebiniz alındı.';
$lang["ar40"] = 'İşleminiz yapılamadı.';
$lang["ar41"] = 'işlem yapılabilir.';
$lang["ar42"] = 'IBAN No';
$lang["ar43"] = 'Banka';
$lang["ar44"] = 'IBAN no eklendi.';
$lang["ar45"] = 'Kayıt başarılı.';
$lang["ar46"] = 'Kaydedilemedi!';
$lang["ar47"] = 'Daha önce eklemişsiniz.';
$lang["ar48"] = 'Durum';
$lang["ar49"] = 'Açıklama';
$lang["ar50"] = 'Yeni Hesap Oluştur';
$lang["ar51"] = 'Balans Tipi';
$lang["ar52"] = 'No';
$lang["ar53"] = 'Açıklama';
$lang["ar54"] = 'Tarih';
$lang["ar55"] = 'TRY Yükle';
$lang["ar56"] = 'TRY Çek';
$lang["ar57"] = 'Seçin';
$lang["ar58"] = '<div class="alert alert-warning" role="alert"><strong>Uyarı!</strong> Para çekebilmeniz için banka IBAN no eklemelisiniz. <a href="'.site_url("bank").'">Tıklayınız</a></div>';
$lang["ar59"] = 'Private Key';
$lang["ar60"] = 'BTC Yükle';
$lang["ar61"] = 'BTC Çek';
$lang["ar62"] = 'Yaklaşık';
$lang["ar63"] = 'IP Adresi';
$lang["ar64"] = 'Modül';
$lang["ar65"] = 'Açık';
$lang["ar66"] = 'Kapalı';

$lang["register"] = 'Parolanız en az 1 büyük harf, 1 küçük harf, 1 özel karakter (!#%@?) içermelidir. En az 8, en fazla 15 uzunlukta bir parola belirlemelisiniz.';

$lang["ticket0"] = "Destek";
$lang["ticket1"] = "e-Posta";
$lang["ticket2"] = "Ad-Soyad";
$lang["ticket3"] = "Telefon Numarası";
$lang["ticket4"] = "Konu";
$lang["ticket5"] = "Açıklama";
$lang["ticket6"] = "Dosya";
$lang["ticket7"] = "Gönder";
$lang["ticket8"] = "Destek Talebi";
$lang["ticket9"] = "Destek talebiniz alındı.";
$lang["ticket10"] = "Destek talebiniz alınamadı. Daha sonra tekrar deneyiniz.";
$lang["ticket11"] = "<div class=\"alert alert-info\"><strong>Durum!</strong> Lütfen talebinizin ayrıntılarını girin. Destek personelinimizin bir üyesi size en kısa zamanda yanıt verecektir.</div>";
$lang["ticket12"] = "İzin verilen dosya türleri png ve jpg'dir. Maksimum 1920 * 1080 px ve 2 mb olabilir.";

$lang["z1"] = "Cinsiyet";
$lang["z2"] = "Erkek";
$lang["z3"] = "Kadın";
$lang["z4"] = "Doğum Tarihi";
$lang["z5"] = "Vatandaşlık No";

$lang["z7"] = "Adres";
$lang["z9"] = "Bilgi Güncelleme";
$lang["z6"] = "Telefon Güncelleme";
$lang["z8"] = "İletişim Güncelleme";
$lang["z10"] = "Banka Güncelleme";
$lang["z11"] = "Banka Güncelleme";
$lang["z12"] = "Belge Güncelleme";

$lang["verify0"] = "Hesap Onay";
$lang["verify1"] = "Onay";
$lang["verify2"] = "Temel Bilgiler";
$lang["verify3"] = "Telefon";
$lang["verify4"] = "İletişim Detayları";
$lang["verify5"] = "Banka Bilgisi";
$lang["verify6"] = "Belge Detayı";

$lang["revize1"] = 'Total';
$lang["revize2"] = 'Onaylanan';
$lang["revize3"] = 'Bekleyen';
$lang["revize4"] = 'Reddedilen';
$lang["revize5"] = 'Belirsiz';

$lang["revize6"] = ' hesabınız var.';
$lang["revize7"] = ' Oluşturulmuş bir balans hesabınız yok! ';
$lang["revize8"] = ' Onay aşamasında olan işlemleriniz var!';
$lang["revize9"] = 'Şimdi Etkinleştir';
$lang["revize10"]= 'Daha Sonra Etkinleştir';

$lang["fmenu1"]= 'Kaynaklar';
$lang["fmenu2"]= 'Hakkımızda';
$lang["fmenu3"]= 'Sosyal Medya';

/*SON*/
$lang["deposit_msg1"] = 'Henüz cüzdan oluşturmamışsınız.';
$lang["deposit_msg2"] = 'Henüz cüzdan oluşturmamışsınız.';
$lang["deposit_msg3"] = 'Cüzdan oluşturuldu!';
$lang["deposit_msg4"] = ' Çekmek istediğiniz miktar ';
$lang["deposit_msg5"] = '\'den az ya da total BTC\'nizden fazla olamaz!';
$lang["deposit_msg6"] = '0.00004 BTC - Yavaş';
$lang["deposit_msg7"] = '0.00006 BTC - Normal';
$lang["deposit_msg8"] = '0.00009 BTC - Hızlı';
$lang["deposit_msg9"] = 'Komisyon Oranı';
$lang["deposit_msg10"] = 'Yeni Cüzdan Oluştur';
$lang["deposit_msg11"] = 'BTC Çek';
$lang["deposit_msg12"] = 'Açıklama';
$lang["deposit_msg13"] = 'Cüzdan ';
$lang["deposit_msg14"] = 'Kaydet';
$lang["deposit_msg15"] = 'Cüzdan Seçiniz';
$lang["deposit_msg16"] = 'Cüzdan oluşturulamadı!';
$lang["deposit_msg17"] = 'Cüzdan eklenemedi!';
$lang["deposit_msg18"] = 'Adres';
$lang["deposit_msg19"] = 'Lütfen yatırma işleminiz için aşağıda belirtilen hesap bilgilerini kullanın ve açıklama bölümüne bu işlem için tanımlanan açıklama kodunu eklemeyi unutmayın. ';
$lang["deposit_msg20"] = 'Banka Hesap Bilgileri';
$lang["deposit_msg21"] = 'Oluştur';
$lang["deposit_msg22"] = 'Kullanılabilir (BTC) total hesabınız yok!';
$lang["deposit_msg23"] = 'En az 100 TRY ile işlem yapabilirsiniz.';
$lang["deposit_msg24"] = 'Kapat';
$lang["deposit_msg25"] = 'Dosya yüklendi.';
$lang["deposit_msg26"] = 'Dosya Seç';
$lang["deposit_msg27"] = '<div class="alert alert-success"><strong>Durum! </strong> Dosya yüklendi. </div>';
$lang["deposit_msg28"] = 'Lütfen kimlik kartınızı ön tarafında tuttuğunuz bir fotoğraf verin. Aynı resimde, Btcoinline\'e bir referans yapın ve bugünün tarihi görüntülenir. Yüzünüzün açıkça görünür olduğundan ve tüm pasaport detaylarının açıkça okunabilir olduğundan emin olun.';
$lang["deposit_msg29"] = '<div class="alert alert-success"><strong>Durum! </strong> Dosya yüklendi. </div>';
$lang["deposit_msg30"] = 'Lütfen kimlik kartınızı ön tarafında tuttuğunuz bir fotoğraf verin. Aynı resimde, Btcoinline\'e bir referans yapın ve bugünün tarihi görüntülenir. Yüzünüzün açıkça görünür olduğundan ve tüm pasaport detaylarının açıkça okunabilir olduğundan emin olun.';
$lang["deposit_msg31"] = '<div class="alert alert-success"><strong>Durum! </strong> Dosya yüklendi. </div>';
$lang["deposit_msg32"] = 'Lütfen kimlik kartınızı ön tarafında tuttuğunuz bir fotoğraf verin. Aynı resimde, Btcoinline\'e bir referans yapın ve bugünün tarihi görüntülenir. Yüzünüzün açıkça görünür olduğundan ve tüm pasaport detaylarının açıkça okunabilir olduğundan emin olun.';
$lang["deposit_msg33"] = 'Kimlik Kartı';
$lang["deposit_msg34"] = 'Pasaport Kartı';
$lang["deposit_msg35"] = 'Sürücü Kartı';
$lang["deposit_msg36"] = 'Kimlik Kartı Yükleme';
