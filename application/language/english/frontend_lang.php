<?php
/*btcoinline.com - english lang param*/
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['test'] = "English  EA";
$lang['m1'] = "Help";
$lang['m2'] = "Login";
$lang['m3'] = "Register";
$lang['m4'] = "Dashboard";
$lang['c1'] = '
                  <div class="col-md-10">
                      <div>
                          <h4>MOBILE WALLET EN</h4>
                          <p>Our popular wallet works on your Android or iPhone in addition to your web browser.</p>
                          <a href="" class="read-more">Read More</a>
                      </div>
                  </div>
                  ';
$lang['c2'] = '
                  <div class="col-md-10">
                      <div>
                          <h4>Secure storage</h4>
                          <p>Our popular wallet works on your Android or iPhone in addition to your web browser.</p>
                          <a href="" class="read-more">Read More</a>
                      </div>
                  </div>
                  ';
$lang['c3'] = '
                  <div class="col-md-10">
                      <div>
                          <h4>Insurance ProtectIon</h4>
                          <p>Our popular wallet works on your Android or iPhone in addition to your web browser.</p>
                          <a href="" class="read-more">Read More</a>
                      </div>
                  </div>
                  ';
$lang['c4'] = '
                  <div class="col-md-10">
                      <div>
                          <h4>Full Control</h4>
                          <p>Our popular wallet works on your Android or iPhone in addition to your web browser.</p>
                          <a href="" class="read-more">Read More</a>
                      </div>
                  </div>
                  ';

$lang['f1'] = "Language";

/*DASHBOARD*/
$lang['dm1'] = "Dashboard";
$lang['dm2'] = "BTC Deposit/Withdraw";
$lang['dm3'] = "TRY Deposit/Withdraw";
$lang['dm4'] = "Bank Accounts";
$lang['dm5'] = "Settings";
$lang['dm6'] = "Security";
$lang['dm7'] = "Account";
$lang['dm8'] = "Password";
$lang['dm9'] = "Logs";
$lang['dm10'] = "Logout";


/*PARTNER*/
$lang['p1'] = "Login";
$lang['p2'] = "E-mail";
$lang['p3'] = "Password";
$lang['p3'] = "Forgot Password ";
$lang['p4'] = "Don't have an account? Register";
$lang['p5'] = "Google Auth Code";
$lang['p6'] = "Login";
$lang['p7'] = "Register an Account";
$lang['p8'] = "Confirm Password";
$lang['p9'] = "Register";
$lang['p10'] = "Verify Activation";
$lang['p11'] = "Verify Forgot";
$lang['p12'] = "Reset Password";
$lang['p13'] = "Password ";

/*Auth Request Alerts*/
$lang['a1'] = "Email / Password incorrect!";
$lang['a2'] = "Please confirm that you are not a robot!";
$lang['a3'] = "Success! Redirecting...";
$lang['a4'] = "Unsuccessful Google 2FA code! ";
$lang['a5'] = "Forgot Password Update";
$lang['a6'] = "Account Activation About";
$lang['a7'] = "Your email account has been approved.";
$lang['a8'] = "Forgot Password";
$lang['a9'] = "Forgot Password";
$lang['a10'] = "Click on the link to renew the password";
$lang['a11'] = "Forgot Password Request";
$lang['a12'] = "Forgot Password Verify";
$lang['a13'] = "Account Verify";
$lang['a14'] = "";
$lang['a15'] = "";
$lang['mail1'] = "Your account has been created";
$lang['mail2'] = "Click on the activation link below to activate your registration";
$lang['mail3'] = "Click";
$lang['mail4'] = "Your registration is successful. You should check your e-mail box and complete the activation process.If you do not see in your inbox, check your spam box.";
$lang['mail5'] = "E-mail could not be sent. Please communicate.";
$lang['mail6'] = "Hash code code could not be generated!. Please communicate.";
$lang['mail7'] = "This email address is registered in the system.";
$lang['mail8'] = "The passwords do not match!";
$lang['mail9'] = "Instructions have been sent to your email address for password renewal. If you do not see in your inbox, check your spam box.";
$lang['mail10'] = "E-mail could not be sent. Please communicate.";
$lang['mail11'] = "Hashcode code could not be generated!. Please communicate.";
$lang['mail12'] = "You are not registered to the system";
$lang['mail13'] = "The account is not approved because the date is past.";
$lang['mail14'] = "Confirmed. You are redirected to the password renewal page.";
$lang['mail15'] = "Unsuccessful. Please try again later!";
$lang['mail16'] = "Forgot Password About";
$lang['mail17'] = 'Your password has been updated.';
$lang['mail18'] = "The password reset operation failed.";
$lang['mail19'] = "This email address not using!";
$lang['mail20'] = "The passwords do not match!";
$lang['mail21'] = "Your mail account has been approved";
$lang['mail22'] = "Your activation could not be done. Try again!";
$lang['mail23'] = "Your password has been updated.";
$lang['mail24'] = "";
$lang['mail25'] = "";

/*Account Request Controller*/
$lang["ar1"] = "First Name";
$lang["ar2"] = "Last Name";
$lang["ar3"] = "Telephone";
$lang["ar4"] = "Country";
$lang["ar5"] = "City";
$lang["ar6"] = "Province/District";
$lang["ar7"] = "Profile Update";
$lang["ar8"] = "Success update!";
$lang["ar9"] = "Unsuccess update!";
$lang["ar10"] = "Save";
$lang["ar11"] = "<strong>Warning!</strong> Once you complete your profile, your account will be upgraded to Advanced. Your BTC and TRY transaction limits will be updated.";
$lang["ar12"] = "<strong>Info!</strong> You have an advanced membership.";
$lang["ar13"] = "Existing Password";
$lang["ar14"] = "New Password";
$lang["ar15"] = "New Password (Again)";
$lang["ar16"] = "Password Update";
$lang["ar17"] = "The passwords do not match!";
$lang["ar18"] = "Your current password is incorrect!";
$lang["ar19"] = "6 Embedded Code";
$lang["ar20"] = "Code does not match!";
$lang["ar21"] = "Code does match!";
$lang["ar22"] = "<strong>Info!</strong> You have a Private membership.";
$lang["ar23"] = "Install Google Authenticator.";
$lang["ar24"] = "Once the QR code has been scanned via Google Authenticator, enter the generated code in the field below.";
$lang["ar25"] = '<div class="alert alert-success">
                      <strong>Status!</strong> Google Two-Factor Authentication activated.
                     </div>';
$lang["ar26"] = '<div class="alert alert-warning">
                      <strong>Status!</strong> Google Two-Factor Authentication is not activated.
                     </div>';

$lang["ar27"] = "E-Mail Activation ";
$lang["ar28"] = "Google 2-Step Session";
$lang["ar29"] = '<div class="alert alert-success">
                      <strong>Status!</strong> You have achieved your e-mail activation.
                    </div>';
$lang["ar30"] = '<div class="alert alert-warning">
                          <strong>Status!</strong> You have not implemented your e-mail activation.
                      </div>
                      <button type="button" id="securityUp2" class="btn btn-primary btn-block">Etkinleştir</button>';
$lang["ar31"] = 'Click the activation link below to complete your registration:';
$lang["ar32"] = 'The activation e-mail was sent to your e-mail address.';
$lang["ar33"] = 'Try again later.';
$lang["ar34"] = 'Notification Type';
$lang["ar35"] = 'Value';
$lang["ar36"] = 'Quantity';
$lang["ar37"] = 'Bank';
$lang["ar38"] = 'Process type';
$lang["ar39"] = 'Your request was received.';
$lang["ar40"] = 'Your transaction could not be made.';
$lang["ar41"] = ' operation can be performed.';
$lang["ar42"] = 'IBAN Number';
$lang["ar43"] = 'Bank';
$lang["ar44"] = 'IBAN no eklendi.';
$lang["ar45"] = 'Successfly saved.';
$lang["ar46"] = 'Unsuccess saved!';
$lang["ar47"] = 'You have added it before.';
$lang["ar48"] = 'Status';
$lang["ar49"] = 'Description';
$lang["ar50"] = 'Create New Bank Account';
$lang["ar51"] = 'Balance Type';
$lang["ar52"] = 'Number';
$lang["ar53"] = 'Description';
$lang["ar54"] = 'Date';
$lang["ar55"] = 'Deposit TRY';
$lang["ar56"] = 'Withdraw TRY';
$lang["ar57"] = 'Select';
$lang["ar58"] = '<div class="alert alert-warning" role="alert"><strong>Warning! </strong>To withdraw money, you must add the bank IBAN number. <a href="'.site_url("bank").'">Click</a></div>';
$lang["ar59"] = 'Private Key';
$lang["ar60"] = 'Deposit BTC';
$lang["ar61"] = 'Withdraw BTC';
$lang["ar62"] = 'Approximately';
$lang["ar63"] = 'IP Address';
$lang["ar64"] = 'Module';
$lang["ar65"] = 'On ';
$lang["ar66"] = 'Off ';

$lang["register"] = 'Your password must contain at least 1 uppercase letter, 1 lowercase letter, 1 special character (!#%@?). You must specify a password of at least 8, at most 15 characters long.';

$lang["ticket0"] = "Support";
$lang["ticket1"] = "e-Mail";
$lang["ticket2"] = "Name, Surname";
$lang["ticket3"] = "Telephone";
$lang["ticket4"] = "Subject";
$lang["ticket5"] = "Description";
$lang["ticket6"] = "File";
$lang["ticket7"] = "Send";
$lang["ticket8"] = "Support Request";
$lang["ticket9"] = "Your support request was received.";
$lang["ticket10"] = "Your support request was not received. Try again later.";
$lang["ticket11"] = "<div class=\"alert alert-info\"><strong>Status!</strong> Please enter details of your request. A member of your support staff will respond to you as soon as possible.</div>";
$lang["ticket12"] = "Allowed file types are png and jpg. The maximum can be 1920 * 1080 px and 2 mb.";

$lang["z1"] = "Gender";
$lang["z2"] = "Male";
$lang["z3"] = "Female";
$lang["z4"] = "Birthday";
$lang["z5"] = "Citizen ID";

$lang["z7"] = "Adress";
$lang["z9"] = "Information Update";
$lang["z6"] = "Phone Number Update";
$lang["z8"] = "Communication Update";
$lang["z10"] = "Bank Update";
$lang["z11"] = "Bank Update";
$lang["z12"] = "Document Update";

$lang["verify0"] = "Account Verification";
$lang["verify1"] = "Verification Menu";
$lang["verify2"] = "Basic Information";
$lang["verify3"] = "Phone Number";
$lang["verify4"] = "Communication Details";
$lang["verify5"] = "Bank Details";
$lang["verify6"] = "Upload Details";

$lang["revize1"] = 'Total';
$lang["revize2"] = 'Accept';
$lang["revize3"] = 'Waiting';
$lang["revize4"] = 'Rejected';
$lang["revize5"] = 'Uncertain';

$lang["revize6"] = ' you have your account.';
$lang["revize7"] = ' You do not have a balancing account created!';
$lang["revize8"] = ' You have transactions that are in the approval phase!';
$lang["revize9"] = ' Activate Now';
$lang["revize10"]= ' Activate Later';

$lang["fmenu1"]= 'Resources';
$lang["fmenu2"]= 'About Us';
$lang["fmenu3"]= 'Social Media';
/*SON*/
$lang["deposit_msg1"] = 'You have not created a wallet yet.';
$lang["deposit_msg2"] = 'You have not created a wallet yet';
$lang["deposit_msg3"] = 'Wallet created!';
$lang["deposit_msg4"] = ' The amount you want to withdraw ';
$lang["deposit_msg5"] = '\' it can not be more or less than your total BTC!';
$lang["deposit_msg6"] = '0.00004 BTC - Slow';
$lang["deposit_msg7"] = '0.00006 BTC - Normal';
$lang["deposit_msg8"] = '0.00009 BTC - Fast';
$lang["deposit_msg9"] = ' Fee';
$lang["deposit_msg10"] = 'Create New Wallet';
$lang["deposit_msg11"] = 'Withdraw BTC';
$lang["deposit_msg12"] = 'Label';
$lang["deposit_msg13"] = 'Wallet ';
$lang["deposit_msg14"] = 'Save';
$lang["deposit_msg15"] = 'Select Wallet';
$lang["deposit_msg16"] = 'Wallet not created!';
$lang["deposit_msg17"] = 'Wallet could not be added!';
$lang["deposit_msg18"] = 'Address';
$lang["deposit_msg19"] = 'Please note that in the calculation and description section, do not add the description code defined for this operation.';
$lang["deposit_msg20"] = 'Bank Account Information';
$lang["deposit_msg21"] = 'Create';
$lang["deposit_msg22"] = 'You do not have a usable (BTC) total account!';
$lang["deposit_msg23"] = 'You can trade at least 100 TRY.';
$lang["deposit_msg24"] = 'Close';
$lang["deposit_msg25"] = 'File uploaded.';
$lang["deposit_msg26"] = 'File Select';
$lang["deposit_msg27"] = '<div class="alert alert-success"><strong>Status!  </strong> File uploaded.</div>';
$lang["deposit_msg28"] = 'Caution!: Please provide a photo of you holding your Identity Card front side. In the same picture, make a reference to Btcoinline and today\'s date displayed. Make sure your face is clearly visible and that all passport details are clearly readable.';
$lang["deposit_msg29"] = '<div class="alert alert-success"><strong>Status!  </strong> File uploaded. </div>';
$lang["deposit_msg30"] = 'Caution!: Please provide a photo of you holding your Identity Card front side. In the same picture, make a reference to Btcoinline and today\'s date displayed. Make sure your face is clearly visible and that all passport details are clearly readable.';
$lang["deposit_msg31"] = '<div class="alert alert-success"><strong>Status!  </strong> File uploaded. </div>';
$lang["deposit_msg32"] = 'Caution!: Please provide a photo of you holding your Identity Card front side. In the same picture, make a reference to Btcoinline and today\'s date displayed. Make sure your face is clearly visible and that all passport details are clearly readable.';
$lang["deposit_msg33"] = 'Identity Card';
$lang["deposit_msg34"] = 'Passport Card';
$lang["deposit_msg35"] = 'Driver Card';
$lang["deposit_msg36"] = 'Identity Card Upload';
