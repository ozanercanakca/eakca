<?php
class LanguageLoader
{

function initialize() {
  $ci =& get_instance();
  $ci->load->helper('language');
  $ci->load->library('session');

  $site_lang = $ci->session->userdata('site_lang');

    if(is_null($site_lang)){
      $ci->session->set_userdata('site_lang','turkish');
    }else{
      $ci->config->set_item('language', $site_lang);
    }

  }
}
