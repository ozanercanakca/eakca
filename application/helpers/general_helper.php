<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function p($data = null, $var_dump = false)
    {
        echo "<pre>";
        if ($var_dump) {
            var_dump($data);
        } else {
            print_r($data);
        }
        echo "</pre>";

    }

    function kisalt($kelime, $str = 50)
    {
        if (strlen($kelime) > $str) {
            if (function_exists("mb_substr")) {
                $kelime = mb_substr($kelime, 0, $str, "UTF-8") . '..';
            } else {
                $kelime = substr($kelime, 0, $str) . '..';
            }
        }
        return $kelime;
    }

    function sef_url($string)
    {
        $string = str_replace(array("ş", "Ş", "ı", "ü", "Ü", "ö", "Ö", "ç", "Ç", "ş", "Ş", "ı", "ğ", "Ğ", "İ", "ö", "Ö", "Ç", "ç", "ü", "Ü"), array("s", "s", "i", "u", "u", "o", "o", "c", "c", "s", "s", "i", "g", "g", "i", "o", "o", "c", "c", "u", "u"), $string);
        return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
    }

    function date_to_tr($time_string = "", $type = 0, $sperator = "")
    {

        if ($time_string && $type == 1) {
            $datetime = new DateTime($time_string);
            $return_time = $datetime->format("d{$sperator}m{$sperator}Y");
        } elseif ($time_string && $type == 2) {
            $datetime = new DateTime($time_string);
            $return_time = $datetime->format("d{$sperator}m{$sperator}Y H:i:s");
        } else {
            $return_time = "";
        }

        return $return_time;

    }

    function clear_string($mVar)
    {
        if (is_array($mVar)) {
            foreach ($mVar as $gVal => $gVar) {
                if (!is_array($gVar)) {
                    $mVar[$gVal] = htmlspecialchars(strip_tags(urldecode(addslashes(stripslashes(stripslashes(trim(htmlspecialchars_decode($gVar))))))));
                } else {
                    $mVar[$gVal] = clear_string($gVar);
                }
            }
        } else {
            $mVar = htmlspecialchars(strip_tags(urldecode(addslashes(stripslashes(stripslashes(trim(htmlspecialchars_decode($mVar))))))));
        }
        return $mVar;


    }

    function lang_abbreviation($lang = "")
    {

        if ($lang == "turkish") {
            $lang = "tr";
        } elseif ($lang == "english") {
            $lang = "en";
        } elseif ($lang == "arabic") {
            $lang = "ar";
        } elseif ($lang == "russian") {
            $lang = "ru";
        } else {
            $lang = "en";
        }

        return $lang;
    }

    function content_image_rename($data = [])
    {

        $key = "9aasdqQ4854dc4d3ed579c869438ff0abfdf";

        $length = 10;
        $code = date("Y-m-d H:i:s") . rand(0, 99999) . $key . $data["user_agent"];
        $out = substr(hash('md5', $code), 0, $length);

        return sha1($out);

    }



