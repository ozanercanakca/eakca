<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_Model extends CI_Model
{

    private $content = 'content';
    private $category = 'category';

    function __construct()
    {
        parent::__construct();
    }

    public function get_category($data = [])
    {

        $this->db->select('*');
        $this->db->where('status', '1');
        $this->db->from('category');

        $query = $this->db->get();

        if ($query->num_rows()) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function get_category_articles_count($id = 0)
    {


        $this->db->select('*');
        $this->db->where('status', '1');
        $this->db->where('category_id', $id);
        $this->db->from('content');

        $query = $this->db->get();

        if ($query->num_rows()) {
            return count($query->result());
        } else {
            return 0;
        }

    }

    public function content_detail($url_tr = "")
    {

        $dil = 'title_tr';

        $this->db->select("
            content.id as content_id,
            content.title_tr as content_title,
            content.url_tr as content_url,
            content.content_tr as content_tr,
            content.category_id,
            content.created_at,
            content.image,
            content.content_read,
            category.id,
            category.$dil as category_name,
            category.url_tr as category_url,
            meta.id as meta_id,
            meta.type,
            meta.parent_id,
            meta.title_tr as meta_title_tr,
            meta.description_tr as meta_description_tr,
            meta.keywords_tr as meta_keywords_tr
        ");
        $this->db->join('category', 'category.id = content.category_id');
        $this->db->join('meta', 'meta.parent_id = content.id');
        $this->db->where('content.status', 1);
        $this->db->where('meta.type', 'content');
        $this->db->where('content.url_tr', $url_tr);

        $query = $this->db->get('content');

        if ($query->num_rows()) {
            return $query->row();
        } else {
            return 0;
        }

    }

    public function get_main_show_content()
    {

        $this->db->select("
            content.id as content_id,
            content.title_tr as content_title,
            content.url_tr as content_url,
            content.content_tr as content_tr,
            content.category_id,
            content.created_at,
            content.image,
            content.main_show,
            category.id as category_id,
            category.title_tr as category_name,
            category.url_tr as category_url
        ");
        $this->db->join('category', 'category.id = content.category_id');
        $this->db->where('content.status', 1);
        $this->db->where('category.status', 1);
        $this->db->where('content.main_show', 1);
        $this->db->limit(2);
        $this->db->from('content');

        $query = $this->db->get();

        if ($query->num_rows()) {
            return $query->result();
        } else {
            return 0;
        }

    }

    public function page_detail($url_tr = "")
    {

        $this->db->select('*');
        $this->db->where('status', 1);
        $this->db->where('url_tr', $url_tr);

        $query = $this->db->get('page');

        if ($query->num_rows()) {
            return $query->row();
        } else {
            return false;
        }

    }

    public function page_meta_detail($id = 0, $type = 'page')
    {

        $this->db->select('*');
        $this->db->where('parent_id', $id);
        $this->db->where('type', $type);

        $query = $this->db->get('meta');

        if ($query->num_rows()) {
            return $query->row();
        } else {
            return false;
        }

    }

    public function get_popular($limit = 0)
    {

        $this->db->select('title_tr,image,created_at,url_tr,content_read');
        $this->db->where('status', '1');
        $this->db->limit($limit);
        $this->db->order_by('content_read', 'desc');
        $this->db->from('content');

        $query = $this->db->get();

        if ($query->num_rows()) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function get_content($limit = "", $offset = "")
    {

        if ($offset > 0) {
            $offset = ($offset - 1) * $limit;
        }

        $this->db->select('
            content.id,
            content.title_tr as content_title,
            content.url_tr as content_url,
            content.summary_tr,
            content.category_id,
            content.created_at,
            content.image,
            category.id,
            category.title_tr as category_name,
            category.url_tr as category_url
        ');
        $this->db->join('category', 'category.id = content.category_id');
        $this->db->where('content.status', 1);
        $this->db->where('category.status', 1);
        $this->db->order_by('content.created_at', 'desc');

        $query = $this->db->get('content',$limit,$offset);

        $result['rows'] = $query->result();

        $affected = $this->db->query("select * from content inner join category on content.category_id=category.id where content.status = '1' && category.status = '1'")->result();
        $result['num_rows'] = count($affected);

        return $result;

    }

    /*public function get_content_search($limit = "", $offset = "", $search = "")
    {

        if ($offset > 0) {
            $offset = ($offset - 1) * $limit;
        }

        $this->db->select('
            content.id,
            content.title_tr as content_title,
            content.url_tr as content_url,
            content.summary_tr,
            content.category_id,
            content.created_at,
            content.image,
            category.id,
            category.title_tr as category_name,
            category.url_tr as category_url
        ');
        $this->db->join('category', 'category.id = content.category_id');
        $this->db->where('content.status', 1);
        $this->db->where('category.status', 1);
        $this->db->like('content.title_tr', $search);
        $this->db->order_by('content.created_at', 'desc');

        $query = $this->db->get('content',$limit,$offset);

        $result['rows'] = $query->result();

        $affected = $this->db->query("select * from content inner join category on content.category_id=category.id where content.status = '1' && category.status = '1' && content.title_tr like '%$search%'")->result();
        $result['num_rows'] = count($affected);

        return $result;

    }*/

    public function get_content_search($search = "")
    {

        $this->db->select('
            content.id,
            content.title_tr as content_title,
            content.url_tr as content_url,
            content.summary_tr,
            content.category_id,
            content.created_at,
            content.image,
            category.id,
            category.title_tr as category_name,
            category.url_tr as category_url
        ');
        $this->db->join('category', 'category.id = content.category_id');
        $this->db->where('content.status', 1);
        $this->db->where('category.status', 1);
        $this->db->like('content.title_tr', $search);
        $this->db->order_by('content.created_at', 'desc');
        $this->db->from('content');

        $query = $this->db->get();

        $result['rows'] = $query->result();

        //$affected = $this->db->query("select * from content inner join category on content.category_id=category.id where content.status = '1' && category.status = '1' && content.title_tr like '%$search%'")->result();
        //$result['num_rows'] = count($affected);

        return $result;

    }

    public function get_category_content($limit = 0, $offset = "", $category_url = "")
    {

        if ($offset > 0) {
            $offset = ($offset - 1) * $limit;
        }

        $this->db->select('
            content.title_tr as content_title,
            content.url_tr as content_url,
            content.summary_tr,
            content.category_id,
            content.created_at,
            content.image,
            category.id,
            category.title_tr as category_name,
            category.url_tr as category_url
        ');
        $this->db->join('content', 'content.category_id = category.id');
        $this->db->where('content.status', 1);
        $this->db->where('category.url_tr', $category_url);
        $this->db->order_by('content.created_at', 'desc');

        $query = $this->db->get('category', $limit, $offset);

        $result['rows'] = $query->result();

        $affected = $this->db->query("select * from category inner join content on category.id = content.category_id where category.status = '1' && content.status = '1' && category.url_tr = '$category_url'")->result();
        $result['num_rows'] = count($affected);

        $category_find = $this->db->query("select id from category where status = '1' && url_tr = '$category_url'")->row();

        if($category_find != null){
            $result['meta'] = $this->db->query("select * from meta where type = 'category' && parent_id = '$category_find->id'")->row();
        }else{
            $result['meta'] = ['title_tr' => '','description_tr' => '', 'keywords' => ''];
        }

        return $result;

    }

    public function content_count_detail($url = "")
    {

        $this->db->select('content_read');
        $this->db->where('url_tr', $url);
        $this->db->where('status', 1);

        $query = $this->db->get('content');

        if ($query->num_rows()) {
            return $query->row();
        } else {
            return false;
        }

    }


    public function content_count_update($data = [])
    {

        if (!is_null($data)) {
            $this->db->where('url_tr', $data['url_tr']);
            return $this->db->update('content', $data);
        }

    }

    public function tag_list($id  = "")
    {

      $this->db->select("
          tag.id as tag_id,
          tag.name_tr,
          tag.name_en,
          tag.url_tr,
          tag.url_en
      ");
      $this->db->join('tag', 'tag.id = content_tag.tag_id');
      $this->db->where('content_tag.status', 1);
      $this->db->where('content_tag.content_id', $id);
      $this->db->from('content_tag');

      $query = $this->db->get();

        if ($query->num_rows()) {
            return $query->result();
        } else {
            return false;
        }

    }

}
