<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: ercan
 * Date: 16.10.2017
 * Time: 11:13
 */

class Userauthreq_Model extends CI_Model{

    function __construct(){
        parent::__construct();
    }

    public function getUser($username = false, $password = false){

        $this->db->select('*');
        $this->db->where('username', $username);
        $this->db->from('user');

        $query = $this->db->get();

        if ($query->num_rows()) { 
            return $query->row();
        } else {
            return 0;
        }

    }

    public function createUser($data = false){

        if($data != false){

            $query = $this->db->insert('user', $data);

            if($query){

                return $this->db->insert_id();

            }else{

                return false;

            }

        }else{

            return false;

        }


    }

    public function createUserHashActivation($data = false){

        if($data != false){

            $query = $this->db->insert('user_activation', $data);

            if($query){

                return $this->db->insert_id();

            }else{

                return false;

            }

        }else{

            return false;

        }


    }

    public function createUserHashForgot($data = false){

        if($data != false){

            $query = $this->db->insert('user_forgot', $data);

            if($query){

                return $this->db->insert_id();

            }else{

                return false;

            }

        }else{

            return false;

        }


    }

    public function userHashForgotDelete($data = []){

        $this->db->where('id', $data['id']);

        return $this->db->update('user_forgot', $data);

    }

    public function getHashCodeCheck($hashCode = "", $uid = ""){

        $this->db->select('*');
        $this->db->where('hashCode', $hashCode);
        $this->db->where('status', '0');
        $this->db->where('userId', $uid);
        $this->db->from('user_activation');

        $query = $this->db->get();

        if ($query->num_rows()) {
            return $query->row();
        } else {
            return false;
        }

    }

    public function getHashCodeIdCheck($uid = ""){

        $this->db->select('*');
        $this->db->where('id_md5', $uid);
        $this->db->from('user');

        $query = $this->db->get();

        if ($query->num_rows()) {
            return $query->row();
        } else {
            return false;
        }

    }

    public function statusUpdate($data = ""){

        if(!is_null($data)){
            $this->db->where('userId', $data['userId']);
            return $this->db->update('user_activation', $data);
        }

    }

    public function confirmUpdate($data = ""){

        if(!is_null($data)){
            $this->db->where('id_md5', $data['id_md5']);
            return $this->db->update('user', $data);
        }

    }

    public function getForgotHashCodeCheck($hashCode = "", $uid = ""){

        $this->db->select('*');
        $this->db->where('hashCode', $hashCode);
        $this->db->where('status', '0');
        $this->db->where('userId', $uid);
        $this->db->from('user_forgot');

        $query = $this->db->get();

        if ($query->num_rows()) {
            return $query->row();
        } else {
            return false;
        }

    }

    public function statusForgotUpdate($data = ""){

        if(!is_null($data)){
            $this->db->where('userId', $data['userId']);
            return $this->db->update('user_forgot', $data);
        }

    }

    public function newPassword($data = []){

        $this->db->where('email', $data['email']);

        return $this->db->update('user', $data);

    }

    public function idMd5HashUpdate($data = ""){

        if(!is_null($data)){
            $this->db->where('id', $data['id']);
            return $this->db->update('user', $data);
        }

    }

    public function idMd5HashCheck($uid = ""){

        $this->db->select('id,id_md5');
        $this->db->where('id_md5', $uid);
        $this->db->from('user');

        $query = $this->db->get();

        if ($query->num_rows()) {
            return $query->row();
        } else {
            return false;
        }

    }

    public function notificationInsert($data = []){

        if(!is_null($data)){
            $query = $this->db->insert_batch('user_notification', $data);
            if($query){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }


    }

    public function firstBankInsert($data = []){

        if(!is_null($data)){
            $query = $this->db->insert_batch('bank_account', $data);
            if($query){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }


    }


}