<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backhome_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function content_create($data = [])
    {
        if (!is_null($data)) {
            $query = $this->db->insert('content', $data);
            if ($query) {
                return $this->db->insert_id();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function content_detail($id = 0)
    {

        $this->db->select('*');
        $this->db->where('id', $id);
        $this->db->from('content');

        $query = $this->db->get();

        if ($query->num_rows()) {
            return $query->row();
        } else {
            return false;
        }

    }

    public function content_update($data = [])
    {

        if (!is_null($data)) {
            $this->db->where('id', $data['id']);
            return $this->db->update('content', $data);
        }

    }

    public function content_delete($data = [])
    {
        if (!is_null($data)) {
            $this->db->where('id', $data['id']);
            return $this->db->delete('content', $data);
        }
    }

    public function category_create($data = [])
    {
        if (!is_null($data)) {
            $query = $this->db->insert('category', $data);
            if ($query) {
                return $this->db->insert_id();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function category_detail($id = 0)
    {

        $this->db->select('*');
        $this->db->where('id', $id);
        $this->db->from('category');

        $query = $this->db->get();

        if ($query->num_rows()) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function category_update($data = [])
    {
        if (!is_null($data)) {
            $this->db->where('id', $data['id']);
            return $this->db->update('category', $data);
        }
    }

    public function category_delete($data = [])
    {
        if (!is_null($data)) {
            $this->db->where('id', $data['id']);
            return $this->db->delete('category', $data);
        }
    }

    public function url_check($data = [])
    {
        $this->db->select('*');
        $this->db->where($data['where']);
        $this->db->from($data['table']);

        $query = $this->db->get();

        if ($query->num_rows()) {
            return true;
        } else {
            return false;
        }

    }

    public function get_category($data = [])
    {
        $this->db->select('*');
        $this->db->where('status', '1');
        $this->db->from('category');

        $query = $this->db->get();

        if ($query->num_rows()) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function get_rank()
    {

        $query = $this->db->query("select max from category where status = '1' order by rank desc limit 1");

        if ($query->num_rows()) {
            return true;
        } else {
            return false;
        }

    }

    public function meta_create($data = [])
    {
        if (!is_null($data)) {
            return $this->db->insert('meta', $data);
        }
    }

    public function meta_update($data = [])
    {

        if (!is_null($data)) {
            $where = "parent_id='{$data['parent_id']}' && type ='{$data['type']}'";
            $this->db->where($where);
            return $this->db->update('meta', $data);
        }

    }

    public function meta_row_update($data = [])
    {

        if (!is_null($data)) {
            $this->db->where('id', $data['id']);
            return $this->db->update('meta', $data);
        }

    }

    public function meta_delete($data = [])
    {
        if (!is_null($data)) {
            return $this->db->delete('meta', $data['where']);
        }
    }

    public function get_meta_detail($data = []){

        $this->db->select('*');
        $this->db->where($data['where']);
        $this->db->from($data['table']);

        $query = $this->db->get();

        if ($query->num_rows()) {
            return $query->row();
        } else {
            return false;
        }

    }

    public function get_content_list($data = [])
    {

        $q = $this->db->query("
              select
                content.id as con_id,content.title_tr,content.url_tr, content.title_en,content.category_id,content.image,content.status,content.created_at,
                category.id as cat_id,category.title_tr as cat_name_tr
              from content
              inner join category on category.id = content.category_id
            ");

        if ($q->num_rows()) {
            return $q->result();
        } else {
            return false;
        }

    }

    public function get_page_list($data = [])
    {

        $q = $this->db->query("
              select * from page
            ");

        if ($q->num_rows()) {
            return $q->result();
        } else {
            return false;
        }

    }

    public function page_detail($id = 0)
    {

        $this->db->select('*');
        $this->db->where('id', $id);
        $this->db->from('page');

        $query = $this->db->get();

        if ($query->num_rows()) {
            return $query->row();
        } else {
            return false;
        }

    }

    public function page_update($data = [])
    {

        if (!is_null($data)) {
            $this->db->where('id', $data['id']);
            return $this->db->update('page', $data);
        }

    }

    public function get_category_list($data = [])
    {

        $q = $this->db->query("
              select
                *
              from category
              ");

        if ($q->num_rows()) {
            return $q->result();
        } else {
            return false;
        }

    }

    public function photo_delete($id = 0, $table='')
    {

        if (!is_null($id) && !is_null($table)) {
            return $this->db->query("update $table set image = '' where id = '$id'");
        }

    }

    public function meta_check($id = 0, $type = '')
    {

        $this->db->select('*');
        $this->db->where('parent_id', $id);
        $this->db->where('type', $type);

        $query = $this->db->get('meta');

        if ($query->num_rows()) {
            return true;
        } else {
            return false;
        }

    }

    public function tag_check($value = '') {

      $this->db->select('*');
      $this->db->where('status', '1');
      $this->db->like('name_tr', $value);
      $this->db->from('tag');

      $query = $this->db->get();

      if ($query->num_rows()) {
        return $query->row("id");
      }else {
        return false;
      }

    }

    public function tag_create($value = '') {

      if ($value != null) {
          $query = $this->db->insert('tag', $value);
          if ($query) {
              return $this->db->insert_id();
          } else {
              return false;
          }
      } else {
          return false;
      }

    }

    public function content_tag_create($data = []) {

      if (!is_null($data)) {
          $query = $this->db->insert_batch('content_tag', $data);
          if ($query) {
              return true;
          } else {
              return false;
          }
      } else {
          return false;
      }

    }


}
