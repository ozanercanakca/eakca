<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LangSwitch extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->helper('url');
        $this->load->helper('language');
    }

    public function switchLanguage($response = [])
    {

        $language = $this->input->post('lang');

        if ($this->input->is_ajax_request() && $this->security->get_csrf_hash() == $this->input->post('token')) {

            if ($language == "turkish" || $language == "english" || $language == "arabic" || $language == "russian") {

                $user_session_data = array('site_lang' => $language);
                $this->session->set_userdata($user_session_data);

                $response = ["status" => true, "redirect" => $_SERVER['HTTP_REFERER']];

            } else {

                $response = ["status" => true, "redirect" => $_SERVER['HTTP_REFERER']];

            }

        } else {
            $response = ["status" => false, "redirect" => ""];
        }

        echo json_encode($response);

    }

}
