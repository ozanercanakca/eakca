<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backhome extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("Backhome_Model");
        $this->load->library("Advanced_login");

        if ($this->advanced_login->get('logged_in') == false) {

            if ($this->advanced_login->get('is_admin') == false)
                redirect(site_url("/index"), 301);
            else
                redirect(site_url("/backhome/index"), 301);

        }
    }

    public function blank($data = [])
    {

        $data = $this->template_load();

        $this->load->view('backend/blank', $data);
    }

    public function index($data = [])
    {

        $data = $this->template_load();

        $this->load->view('backend/index', $data);
    }

    public function content_create($data = [])
    {

        $data = $this->template_load();

        $data["category"] = $this->Backhome_Model->get_category();

        $this->load->view('backend/content_create', $data);
    }

    public function content_edit($data = [])
    {

        $data = $this->template_load();

        $data["category"] = $this->Backhome_Model->get_category();

        $data["content_detail"] = $this->Backhome_Model->content_detail($this->uri->segment(4));

        if ($data["content_detail"] === false) {
            redirect(site_url("backhome/index"));
        }

        $this->load->view('backend/content_edit', $data);
    }

    public function content_list($data = [])
    {

        $data = $this->template_load();

        $this->load->view('backend/content_list', $data);
    }

    public function category_create($data = [])
    {

        $data = $this->template_load();

        $this->load->view('backend/category_create', $data);
    }

    public function category_edit($data = [])
    {

        $data = $this->template_load();

        $data["category_detail"] = $this->Backhome_Model->category_detail($this->uri->segment(4));

        if ($data["category_detail"] === false) {
            redirect(site_url("backhome/index"));
        }

        $this->load->view('backend/category_edit', $data);
    }

    public function category_list($data = [])
    {

        $data = $this->template_load();

        $this->load->view('backend/category_list', $data);
    }

    public function page_list($data = [])
    {

        $data = $this->template_load();

        $this->load->view('backend/page_list', $data);
    }

    public function page_edit($data = [])
    {

        $data = $this->template_load();

        $data["page_detail"] = $this->Backhome_Model->page_detail($this->uri->segment(4));

        if ($data["page_detail"] === false) {
            redirect(site_url("backhome/index"));
        }

        $this->load->view('backend/page_edit', $data);
    }

    public function template_load($data = [])
    {

        $data['head'] = $this->backend->head();
        $data['header_fix'] = $this->backend->header_fix();
        $data['main_top_bar'] = $this->backend->main_top_bar();
        $data['side_bar'] = $this->backend->side_bar();
        $data['side_chat'] = $this->backend->side_chat();

        $data['js'] = $this->backend->js();

        $data["csrf"] = ['name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash()];

        return $data;
    }


}
