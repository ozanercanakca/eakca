<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backhome_request extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model("Backhome_Model");
        $this->load->library('user_agent');
        $this->load->library('upload');

            $response = [];

        if($this->session->userdata("logged_in") != '1'){
            redirect(site_url());
        }

    }

    public function blank()
    {
        $this->load->view('backend/blank');
    }

    public function response_send($response = [])
    {

        if (isset($response))
            echo json_encode($response);
        else
            echo json_encode(['status' => "false!"]);

    }

    public function content_create($response = [])
    {

        if ($this->input->is_ajax_request() && $this->security->get_csrf_hash() == $this->input->post('csrf_token_oea')) {

            $data = [];

            $set_rules = [
                [
                    'field' => 'title_tr',
                    'label' => 'Title TR',
                    'rules' => 'trim|required|strip_tags'
                ],
                [
                    'field' => 'summary_tr',
                    'label' => 'Summary TR',
                    'rules' => 'trim|required|strip_tags'
                ],
                [
                    'field' => 'content_tr',
                    'label' => 'Content TR',
                    'rules' => 'trim|required'
                ],
                [
                    'field' => 'category_id',
                    'label' => 'Category ID',
                    'rules' => 'trim|required'
                ],
                [
                    'field' => 'tag_list',
                    'label' => 'Etiket',
                    'rules' => 'trim|strip_tags'
                ],
                [
                    'field' => 'status',
                    'label' => 'Status',
                    'rules' => 'trim|required|strip_tags'
                ]
            ];

            $this->form_validation->set_rules($set_rules);

            if ($this->form_validation->run() === false) {

                $response = [
                    'status' => false,
                    'content' => validation_errors(),
                    'redirect' => ''
                ];

            } else {

                $data = $this->security->xss_clean($this->input->post(), true);
                $tag_list = $this->input->post('tag_list');

                unset($data['csrf_token_oea']);
                unset($data['tag_list']);

                $data['title_en'] = ($data['title_en'] == null ? $data['title_tr'] : $data['title_en']);
                $data['content_en'] = ($data['content_en'] == null ? $data['content_tr'] : $data['content_en']);
                $data['summary_en'] = ($data['summary_en'] == null ? $data['summary_tr'] : $data['summary_en']);
                $data['url_tr'] = sef_url($data['title_tr']);
                $data['url_en'] = sef_url($data['title_en']);

                $where = "url_tr='{$data['url_tr']}' || url_en='{$data['url_en']}'";

                $url_check = $this->Backhome_Model->url_check(['where' => $where, 'table' => 'content']);

                if ($url_check === true) {

                    $response = [
                        'status' => false,
                        'content' => 'Daha önce kaydedilmiş!',
                        'redirect' => ''
                    ];

                } else {

                    if (isset($_FILES["image"]["name"])) {

                        $file = $_FILES["image"];

                        if ($this->security->xss_clean($file, TRUE) !== FALSE) {

                            $config['upload_path'] = FCPATH . 'upload/content';

                            $config['allowed_types'] = 'jpg|png';
                            $config['max_size'] = 4096; // 4mb
                            $config['max_width'] = 1920;
                            $config['max_height'] = 1080;
                            $config['file_name'] = content_image_rename(["user_agent" => $this->input->user_agent()]);

                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('image')) {
                                $response = [
                                    'status' => false,
                                    'content' => $this->upload->display_errors(),
                                    'redirect' => ''
                                ];
                                $this->response_send($response);
                                exit();
                            } else {

                                $data['image'] = $this->upload->data('file_name');

                                $tothumb['image_library'] = 'gd2';
                                $tothumb['source_image'] = $this->config->item("source_image_content"). $data['image'];
                                $tothumb['create_thumb'] = TRUE;
                                $tothumb['maintain_ratio'] = TRUE;
                                $tothumb['width'] = 800;
                                $tothumb['height'] = 533;
                                $tothumb['new_image'] = $this->config->item("source_image_content");

                                $this->load->library('image_lib');
                                $this->image_lib->initialize($tothumb);

                                if (!$this->image_lib->resize()) {
                                    echo $this->image_lib->display_errors();
                                    $response = [
                                        'status' => false,
                                        'content' => 'false!',
                                        'redirect' => ''
                                    ];
                                    $this->response_send($response);
                                    exit();
                                } else {
                                    $this->image_lib->clear();
                                }

                            }

                        } else {
                            $response = [
                                'status' => false,
                                'content' => 'false!',
                                'redirect' => ''
                            ];
                        }

                    } else {
                        $data['image'] = '';
                    }

                    $insert = $this->Backhome_Model->content_create($data);

                    if ($insert) {

                        $this->Backhome_Model->meta_create(['parent_id' => $insert, 'title_tr' => $data['title_tr'], 'title_en' => $data['title_en'], 'type' => 'content']);

                        if($tag_list != NULL){
                        $list = explode (',', $tag_list);
                        }

                        $tag_data = [];

                        foreach ($list as $key => $value) {

                          $check = $this->Backhome_Model->tag_check($value);

                          if($check == true){
                            $tag_data[] = ["content_id" => $insert, "tag_id" => $check];
                          }else {
                            $create = $this->Backhome_Model->tag_create(["name_tr" => $value, "url_tr" => sef_url($value)]);
                            if($create == true){
                              $tag_data[] = ["content_id" => $insert, "tag_id" => $create];
                            }
                          }

                        }

                        $this->Backhome_Model->content_tag_create($tag_data);

                        $response = [
                            'status' => true,
                            'content' => 'Başarılı!',
                            'redirect' => site_url('backhome/content/list')
                        ];

                    } else {
                        $response = [
                            'status' => false,
                            'content' => 'Başarısız!',
                            'redirect' => ''
                        ];
                    }

                }

            }
        }

        $this->response_send($response);

    }

    public function content_update($response = [])
    {

        if ($this->input->is_ajax_request() && $this->security->get_csrf_hash() == $this->input->post('csrf_token_oea')) {

            $data = [];

            $set_rules = [
                [
                    'field' => 'id',
                    'label' => 'ID',
                    'rules' => 'trim|required|strip_tags'
                ],
                [
                    'field' => 'title_tr',
                    'label' => 'Title TR',
                    'rules' => 'trim|required|strip_tags'
                ],
                [
                    'field' => 'summary_tr',
                    'label' => 'Summary TR',
                    'rules' => 'trim|required|strip_tags'
                ],
                [
                    'field' => 'content_tr',
                    'label' => 'Content TR',
                    'rules' => 'trim|required'
                ],
                [
                    'field' => 'category_id',
                    'label' => 'Category ID',
                    'rules' => 'trim|required'
                ],
                [
                    'field' => 'status',
                    'label' => 'Status',
                    'rules' => 'trim|required|strip_tags'
                ]
            ];

            $this->form_validation->set_rules($set_rules);

            if ($this->form_validation->run() === false) {

                $response = [
                    'status' => false,
                    'content' => validation_errors(),
                    'redirect' => ''
                ];

            } else {

                $data = $this->security->xss_clean($this->input->post(), true);
                unset($data['csrf_token_oea']);

                $data['title_en'] = ($data['title_en'] == null ? $data['title_tr'] : $data['title_en']);
                $data['content_en'] = ($data['content_en'] == null ? $data['content_tr'] : $data['content_en']);
                $data['summary_en'] = ($data['summary_en'] == null ? $data['summary_tr'] : $data['summary_en']);
                $data['url_tr'] = sef_url($data['title_tr']);
                $data['url_en'] = sef_url($data['title_en']);

                $where = " id !='{$data['id']}' && (url_tr='{$data['url_tr']}' || url_en='{$data['url_en']}')";

                $url_check = $this->Backhome_Model->url_check(['where' => $where, 'table' => 'content']);

                if ($url_check === true) {

                    $response = [
                        'status' => false,
                        'content' => 'Daha önce kaydedilmiş!',
                        'redirect' => ''
                    ];

                } else {

                    if (isset($_FILES["image"]["name"])) {

                        $file = $_FILES["image"];

                        if ($this->security->xss_clean($file, TRUE) !== FALSE) {

                            $config['upload_path'] = FCPATH . 'upload/content';

                            $config['allowed_types'] = 'jpg|png';
                            $config['max_size'] = 4096; // 4mb
                            $config['max_width'] = 1920;
                            $config['max_height'] = 1080;
                            $config['file_name'] = content_image_rename(["user_agent" => $this->input->user_agent()]);

                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('image')) {
                                $response = [
                                    'status' => false,
                                    'content' => $this->upload->display_errors(),
                                    'redirect' => ''
                                ];
                                $this->response_send($response);
                                exit();
                            } else {

                                $data['image'] = $this->upload->data('file_name');

                                $tothumb['image_library'] = 'gd2';
                                $tothumb['source_image'] = $this->config->item("source_image_content") . $data['image'];
                                $tothumb['create_thumb'] = TRUE;
                                $tothumb['maintain_ratio'] = TRUE;
                                $tothumb['width'] = 800;
                                $tothumb['height'] = 533;
                                $tothumb['new_image'] = $this->config->item("source_image_content");

                                $this->load->library('image_lib');
                                $this->image_lib->initialize($tothumb);

                                if (!$this->image_lib->resize()) {
                                    echo $this->image_lib->display_errors();
                                    $response = [
                                        'status' => false,
                                        'content' => 'false!',
                                        'redirect' => ''
                                    ];
                                    $this->response_send($response);
                                    exit();
                                } else {
                                    $this->image_lib->clear();
                                }

                            }

                        } else {
                            $response = [
                                'status' => false,
                                'content' => 'false!',
                                'redirect' => ''
                            ];
                        }

                    } else {
                        $get_photo = $this->db->query("select * from content where id = '{$data['id']}'")->row();

                        if ($get_photo != null) {
                            $data['image'] = $get_photo->image;
                        }
                    }

                    $data['updated_at'] = date('Y-m-d H:i:s');

                    $update = $this->Backhome_Model->content_update($data);

                    if ($update) {

                        if( $this->Backhome_Model->meta_check($data['id'],'content') == false){

                            $this->Backhome_Model->meta_create(['parent_id' => $data['id'], 'title_tr' => $data['title_tr'], 'title_en' => $data['title_en'], 'type' => 'content']);
                            //$this->Backhome_Model->meta_update(['parent_id' => $data['id'], 'title_tr' => $data['title_tr'], 'title_en' => $data['title_en'], 'type' => 'content']);

                        }

                        $response = [
                            'status' => true,
                            'content' => 'Başarılı!',
                            'redirect' => site_url('backhome/content/list')
                        ];

                    } else {
                        $response = [
                            'status' => false,
                            'content' => 'Başarısız!',
                            'redirect' => ''
                        ];
                    }

                }

            }
        }

        $this->response_send($response);

    }

    public function content_delete($response = [])
    {

        if ($this->input->is_ajax_request()/* && $this->security->get_csrf_hash() == $this->input->post('csrf_token_oea')*/) {

            $data = [];

            $set_rules = [
                [
                    'field' => 'id',
                    'label' => 'ID',
                    'rules' => 'trim|required|strip_tags'
                ]
            ];

            $this->form_validation->set_rules($set_rules);

            if ($this->form_validation->run() === false) {

                $response = [
                    'status' => false,
                    'content' => validation_errors(),
                    'redirect' => ''
                ];

            } else {

                $data = $this->security->xss_clean($this->input->post(), true);
                unset($data['csrf_token_oea']);

                $delete = $this->Backhome_Model->content_delete($data);
                $meta_delete = $this->Backhome_Model->meta_delete(['where' => " parent_id='{$data['id']}' && type='content'"]);

                if ($delete) {

                    $response = [
                        'status' => true,
                        'content' => 'Başarılı!',
                        'redirect' => site_url('backhome/content/list')
                    ];

                } else {
                    $response = [
                        'status' => false,
                        'content' => 'Başarısız!',
                        'redirect' => ''
                    ];
                }

            }
        }

        $this->response_send($response);

    }

    public function get_content_list($response = [])
    {

        if ($this->input->is_ajax_request()) {

            $get_content_list = $this->Backhome_Model->get_content_list();

            if ($get_content_list) {

                foreach ($get_content_list as $item) {

                    $status = ($item->status == '1' ? '<span class="badge badge-success text-white">Active</span>' : '<span class="badge badge-warning text-white">Passive</span>');
                    $image = $item->image != '' ? '<img src="' . site_url("upload/content/$item->image") . '" style="max-width: 100px !important;"/>' : '';

                    $response["data"][] =
                        [
                            $item->con_id,
                            $image,
                            '<a href="'.site_url("detail/$item->url_tr").'" target="_blank">'.kisalt($item->title_tr, 50).'</a>',
                            kisalt($item->title_en, 50),
                            $item->cat_name_tr,
                            date_to_tr($item->created_at, 1, "/"),
                            $status,
                            '<div class="btn-group mb-2"><a href="' . site_url("backhome/content/edit/") . '' . $item->con_id . '" class="btn btn-outline-primary">Edit</a></div>
                             <div class="btn-group mb-2"><div class="btn btn-outline-warning" onclick="meta_detail(' . $item->con_id . ')">Meta</div></div>
                             <div class="btn-group mb-2"><div class="btn btn-outline-danger" onclick="content_delete(' . $item->con_id . ')">Delete</div></div>'
                        ];
                }
            }else{
                $response = ["data" => [], "length" => 0];
            }

        }else{
            $response = ["data" => [], "length" => 0];
        }

        $this->response_send($response);

    }

    public function get_page_list($response = [])
    {

        if ($this->input->is_ajax_request()) {

            $get_page_list = $this->Backhome_Model->get_page_list();

            if ($get_page_list) {

                foreach ($get_page_list as $item) {

                    $status = ($item->status == '1' ? '<span class="badge badge-success text-white">Active</span>' : '<span class="badge badge-warning text-white">Passive</span>');
                    $image = $item->image != '' ? '<img src="' . site_url("upload/page/$item->image") . '" style="max-width: 100px !important;"/>' : '';

                    $response["data"][] =
                        [
                            $item->id,
                            $image,
                            '<a href="'.site_url("me/$item->url_tr").'" target="_blank">'.kisalt($item->title_tr, 50).'</a>',
                            kisalt($item->title_en, 50),
                            date_to_tr($item->created_at, 1, "/"),
                            $status,
                            '<div class="btn-group mb-2"><a href="' . site_url("backhome/page/edit/") . '' . $item->id . '" class="btn btn-outline-primary">Edit</a></div>
                             <div class="btn-group mb-2"><div class="btn btn-outline-warning" onclick="meta_detail(' . $item->id . ')">Meta</div></div>'
                        ];
                }
            }else{
                $response = ["data" => [], "length" => 0];
            }

        }else{
            $response = ["data" => [], "length" => 0];
        }

        $this->response_send($response);

    }

    public function category_create($response = [])
    {

        if ($this->input->is_ajax_request() && $this->security->get_csrf_hash() == $this->input->post('csrf_token_oea')) {

            $data = [];

            $set_rules = [
                [
                    'field' => 'title_tr',
                    'label' => 'Title TR',
                    'rules' => 'trim|required|strip_tags'
                ],
                [
                    'field' => 'status',
                    'label' => 'Status',
                    'rules' => 'trim|required|strip_tags'
                ]
            ];

            $this->form_validation->set_rules($set_rules);

            if ($this->form_validation->run() == FALSE) {

                $response = [
                    'status' => false,
                    'content' => validation_errors(),
                    'redirect' => ''
                ];

            } else {

                $data = $this->security->xss_clean($this->input->post(), true);
                unset($data['csrf_token_oea']);

                $data['url_tr'] = sef_url($data['title_tr']);
                $data['url_en'] = sef_url($data['title_en']);
                $data['title_en'] = ($data['title_en'] == null ? $data['title_tr'] : $data['title_en']);
                $data['url_en'] = ($data['url_en'] == null ? $data['url_tr'] : $data['url_en']);

                $where = "url_tr='{$data['url_tr']}' || url_en='{$data['url_en']}'";

                $url_check = $this->Backhome_Model->url_check(['where' => $where, 'table' => 'category']);

                if ($url_check === true) {

                    $response = [
                        'status' => false,
                        'content' => 'Daha önce kaydedilmiş!',
                        'redirect' => ''
                    ];

                } else {

                    $insert = $this->Backhome_Model->category_create($data);

                    if ($insert) {

                        $this->Backhome_Model->meta_create(['parent_id' => $insert, 'title_tr' => $data['title_tr'], 'title_en' => $data['title_en'], 'type' => 'category']);

                        $response = [
                            'status' => true,
                            'content' => 'Başarılı!',
                            'redirect' => site_url('backhome/category/list')
                        ];

                    } else {

                        $response = [
                            'status' => false,
                            'content' => 'Başarısız!',
                            'redirect' => ''
                        ];

                    }

                }

            }
        }

        $this->response_send($response);

    }

    public function category_update($response = [])
    {

        if ($this->input->is_ajax_request() && $this->security->get_csrf_hash() == $this->input->post('csrf_token_oea')) {

            $set_rules = [
                [
                    'field' => 'title_tr',
                    'label' => 'Title TR',
                    'rules' => 'trim|required|strip_tags'
                ],
                [
                    'field' => 'status',
                    'label' => 'Status',
                    'rules' => 'trim|required|strip_tags'
                ]
            ];

            $this->form_validation->set_rules($set_rules);

            if ($this->form_validation->run() === false) {

                $response = [
                    'status' => false,
                    'content' => validation_errors(),
                    'redirect' => ''
                ];

            } else {

                $data = $this->security->xss_clean($this->input->post(), true);
                unset($data['csrf_token_oea']);

                $data['title_en'] = ($data['title_en'] == null ? $data['title_tr'] : $data['title_en']);
                $data['url_tr'] = sef_url($data['title_tr']);
                $data['url_en'] = sef_url($data['title_en']);

                $where = " id !='{$data['id']}' && (url_tr='{$data['url_tr']}' || url_en='{$data['url_en']}')";

                $url_check = $this->Backhome_Model->url_check(['where' => $where, 'table' => 'category']);

                if ($url_check === true) {

                    $response = [
                        'status' => false,
                        'content' => 'Daha önce kaydedilmiş!',
                        'redirect' => ''
                    ];

                } else {

                    $data['updated_at'] = date('Y-m-d H:i:s');

                    $update = $this->Backhome_Model->category_update($data);

                    if ($update) {

                        if( $this->Backhome_Model->meta_check($data['id'],'category') == false){

                            $this->Backhome_Model->meta_create(['parent_id' => $data['id'], 'title_tr' => $data['title_tr'], 'title_en' => $data['title_en'], 'type' => 'category']);

                        }

                        $response = [
                            'status' => true,
                            'content' => 'Başarılı!',
                            'redirect' => site_url('backhome/category/list')
                        ];

                    } else {
                        $response = [
                            'status' => false,
                            'content' => 'Başarısız!',
                            'redirect' => ''
                        ];
                    }

                }

            }
        }

        $this->response_send($response);

    }

    public function category_delete($response = [])
    {

        if ($this->input->is_ajax_request()/* && $this->security->get_csrf_hash() == $this->input->post('csrf_token_oea')*/) {

            $data = [];

            $set_rules = [
                [
                    'field' => 'id',
                    'label' => 'ID',
                    'rules' => 'trim|required|strip_tags'
                ]
            ];

            $this->form_validation->set_rules($set_rules);

            if ($this->form_validation->run() === false) {

                $response = [
                    'status' => false,
                    'content' => validation_errors(),
                    'redirect' => ''
                ];

            } else {

                $data = $this->security->xss_clean($this->input->post(), true);
                unset($data['csrf_token_oea']);

                $delete = $this->Backhome_Model->category_delete($data);

                $meta_delete = $this->Backhome_Model->meta_delete(['where' => " parent_id='{$data['id']}' && type='category'"]);

                if ($delete) {

                    $response = [
                        'status' => true,
                        'content' => 'Başarılı!',
                        'redirect' => site_url('backhome/category/list')
                    ];

                } else {
                    $response = [
                        'status' => false,
                        'content' => 'Başarısız!',
                        'redirect' => ''
                    ];
                }

            }
        }

        $this->response_send($response);

    }

    public function get_category_list($response = [])
    {

        if ($this->input->is_ajax_request()) {

            $get_category_list = $this->Backhome_Model->get_category_list();

            if ($get_category_list) {
                foreach ($get_category_list as $item) {

                    $status = ($item->status == '1' ? '<span class="badge badge-success text-white">Active</span>' : '<span class="badge badge-warning text-white">Passive</span>');

                    $response["data"][] =
                        [
                            $item->id,
                            '<a href="'.site_url("category/$item->url_tr").'" target="_blank">'.kisalt($item->title_tr, 50).'</a>',
                            $item->title_en,
                            date_to_tr($item->created_at, 1, "/"),
                            $status,
                            '<div class="btn-group mb-2"><a href="' . site_url("backhome/category/edit/") . '' . $item->id . '" class="btn btn-outline-primary">Edit</a></div>
                             <div class="btn-group mb-2"><div class="btn btn-outline-warning" onclick="meta_detail(' . $item->id . ')">Meta</div></div>
                             <div class="btn-group mb-2"><div class="btn btn-outline-danger" onclick="category_delete(' . $item->id . ')">Delete</div></div>
                             '
                        ];
                }
            }else{
                $response = ["data" => [], "length" => 0];
            }


        }else{
            $response = ["data" => [], "length" => 0];
        }


        $this->response_send($response);

    }

    public function meta_detail($response = [])
    {
        if ($this->input->is_ajax_request() /*&& $this->security->get_csrf_hash() == $this->input->post('csrf_token_oea')*/) {

            $data = [];

            $set_rules = [
                [
                    'field' => 'id',
                    'label' => 'ID',
                    'rules' => 'trim|required|strip_tags'
                ]
            ];

            $this->form_validation->set_rules($set_rules);

            if ($this->form_validation->run() === false) {

                $response = [
                    'status' => false,
                    'alert' => 'alert alert-danger',
                    'content' => strip_tags(validation_errors()),
                    'redirect' => ''
                ];

            } else {

                $data = $this->security->xss_clean($this->input->post(), true);
                unset($data['csrf_token_oea']);


                $detail = $this->Backhome_Model->get_meta_detail(['where' => " parent_id='{$data['id']}' && type='{$data['type']}'", "table" => "meta"]);

                if ($detail) {

                    $response = [
                        'status' => true,
                        'content' =>
                            [
                                'id' => $detail->id,
                                'title_tr' => $detail->title_tr,
                                'title_en' => $detail->title_en,
                                'description_tr' => $detail->description_tr,
                                'description_en' => $detail->description_en,
                                'keywords_tr' => $detail->keywords_tr,
                                'keywords_en' => $detail->keywords_en
                            ],
                        'redirect' => ''
                    ];

                } else {
                    $response = [
                        'status' => false,
                        'content' => 'Meta kaydı silinmiş, kaydı düzenleyerek yeniden oluşturabilirsiniz.',
                        'redirect' => ''
                    ];
                }

            }
        }

        $this->response_send($response);

    }

    public function page_update($response = [])
    {

        if ($this->input->is_ajax_request() && $this->security->get_csrf_hash() == $this->input->post('csrf_token_oea')) {

            $data = [];

            $set_rules = [
                [
                    'field' => 'id',
                    'label' => 'ID',
                    'rules' => 'trim|required|strip_tags'
                ],
                [
                    'field' => 'title_tr',
                    'label' => 'Title TR',
                    'rules' => 'trim|required|strip_tags'
                ],
                [
                    'field' => 'content_tr',
                    'label' => 'Content TR',
                    'rules' => 'trim|required'
                ],
                [
                    'field' => 'status',
                    'label' => 'Status',
                    'rules' => 'trim|required|strip_tags'
                ]
            ];

            $this->form_validation->set_rules($set_rules);

            if ($this->form_validation->run() === false) {

                $response = [
                    'status' => false,
                    'content' => validation_errors(),
                    'redirect' => ''
                ];

            } else {

                //$data = $this->security->xss_clean($this->input->post(), true);
                $data = $this->security->xss_clean($this->input->post());

                unset($data['csrf_token_oea']);

                $data['title_en'] = ($data['title_en'] == null ? $data['title_tr'] : $data['title_en']);
                $data['content_en'] = ($data['content_en'] == null ? $data['content_tr'] : $data['content_en']);
                $data['url_tr'] = sef_url($data['title_tr']);
                $data['url_en'] = sef_url($data['title_en']);

                $where = " id !='{$data['id']}' && (url_tr='{$data['url_tr']}' || url_en='{$data['url_en']}')";

                $url_check = $this->Backhome_Model->url_check(['where' => $where, 'table' => 'page']);

                if ($url_check === true) {

                    $response = [
                        'status' => false,
                        'content' => 'Daha önce kaydedilmiş!',
                        'redirect' => ''
                    ];

                } else {

                    if (isset($_FILES["image"]["name"])) {

                        $file = $_FILES["image"];

                        if ($this->security->xss_clean($file, TRUE) !== FALSE) {

                            $config['upload_path'] = FCPATH . 'upload/page';

                            $config['allowed_types'] = 'jpg|png';
                            $config['max_size'] = 4096; // 4mb
                            $config['max_width'] = 1920;
                            $config['max_height'] = 1080;
                            $config['file_name'] = content_image_rename(["user_agent" => $this->input->user_agent()]);

                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('image')) {
                                $response = [
                                    'status' => false,
                                    'content' => $this->upload->display_errors(),
                                    'redirect' => ''
                                ];
                                $this->response_send($response);
                                exit();
                            } else {

                                $data['image'] = $this->upload->data('file_name');

                                $tothumb['image_library'] = 'gd2';
                                $tothumb['source_image'] = $this->config->item("source_image_page"). $data['image'];
                                $tothumb['create_thumb'] = TRUE;
                                $tothumb['maintain_ratio'] = TRUE;
                                $tothumb['width'] = 800;
                                $tothumb['height'] = 533;
                                $tothumb['new_image'] = $this->config->item("source_image_page");

                                $this->load->library('image_lib');
                                $this->image_lib->initialize($tothumb);

                                if (!$this->image_lib->resize()) {
                                    echo $this->image_lib->display_errors();
                                    $response = [
                                        'status' => false,
                                        'content' => 'false!',
                                        'redirect' => ''
                                    ];
                                    $this->response_send($response);
                                    exit();
                                } else {
                                    $this->image_lib->clear();
                                }

                            }

                        } else {
                            $response = [
                                'status' => false,
                                'content' => 'false!',
                                'redirect' => ''
                            ];
                        }

                    } else {

                        $get_photo = $this->db->query("select * from page where id = '{$data['id']}'")->row();

                        if ($get_photo != null) {
                            $data['image'] = $get_photo->image;
                        }
                    }

                    $data['updated_at'] = date('Y-m-d H:i:s');

                    $update = $this->Backhome_Model->page_update($data);

                    if ($update) {

                        if( $this->Backhome_Model->meta_check($data['id'],'page') == false){

                            $this->Backhome_Model->meta_create(['parent_id' => $data['id'], 'title_tr' => $data['title_tr'], 'title_en' => $data['title_en'], 'type' => 'page']);

                        }

                        $response = [
                            'status' => true,
                            'content' => 'Başarılı!',
                            'redirect' => site_url('backhome/page/list')
                        ];

                    } else {
                        $response = [
                            'status' => false,
                            'content' => 'Başarısız!',
                            'redirect' => ''
                        ];
                    }

                }

            }
        }

        $this->response_send($response);

    }

    public function meta_update($response = [])
    {

        if ($this->input->is_ajax_request()/* && $this->security->get_csrf_hash() == $this->input->post('csrf_token_oea')*/) {

            $data = [];

            $set_rules = [
                [
                    'field' => 'id',
                    'label' => 'ID',
                    'rules' => 'trim|required|strip_tags'
                ],
                [
                    'field' => 'title_tr',
                    'label' => 'Title TR',
                    'rules' => 'trim|required|strip_tags'
                ],
                [
                    'field' => 'title_en',
                    'label' => 'Title EN',
                    'rules' => 'trim|strip_tags'
                ],
                [
                    'field' => 'description_tr',
                    'label' => 'Description TR',
                    'rules' => 'trim|strip_tags'
                ],
                [
                    'field' => 'description_en',
                    'label' => 'Description EN',
                    'rules' => 'trim|strip_tags'
                ],
                [
                    'field' => 'keywords_tr',
                    'label' => 'Keywords TR',
                    'rules' => 'trim|strip_tags'
                ],
                [
                    'field' => 'keywords_en',
                    'label' => 'Keywords EN',
                    'rules' => 'trim|strip_tags'
                ]
            ];

            $this->form_validation->set_rules($set_rules);

            if ($this->form_validation->run() === false) {

                $response = [
                    'status' => false,
                    'content' => strip_tags(validation_errors()),
                    'redirect' => ''
                ];

            } else {

                $data = $this->security->xss_clean($this->input->post(), true);

                $data['title_en'] = ($data['title_en'] == null ? $data['title_tr'] : $data['title_en']);
                $data['description_en'] = ($data['description_en'] == null ? $data['description_tr'] : $data['description_en']);
                $data['keywords_en'] = ($data['keywords_en'] == null ? $data['keywords_tr'] : $data['keywords_en']);

                $data['updated_at'] = date('Y-m-d H:i:s');

                $update = $this->Backhome_Model->meta_row_update($data);

                if ($update) {

                    $response = [
                        'status' => true,
                        'content' => 'Başarılı!',
                        'redirect' => ''
                    ];

                } else {
                    $response = [
                        'status' => false,
                        'content' => 'Başarısız!',
                        'redirect' => ''
                    ];
                }

            }
        }

        $this->response_send($response);

    }

    public function photo_delete($response = [])
    {

        if ($this->input->is_ajax_request()/* && $this->security->get_csrf_hash() == $this->input->post('csrf_token_oea')*/) {

            $data = [];

            $set_rules = [
                [
                    'field' => 'id',
                    'label' => 'ID',
                    'rules' => 'trim|required|strip_tags'
                ],
                [
                    'field' => 'type',
                    'label' => 'Type',
                    'rules' => 'trim|required|strip_tags'
                ]
            ];

            $this->form_validation->set_rules($set_rules);

            if ($this->form_validation->run() === false) {

                $response = [
                    'status' => false,
                    'content' => strip_tags(validation_errors()),
                    'redirect' => ''
                ];

            } else {

                $data = $this->security->xss_clean($this->input->post(), true);

                /*dizinden sil*/

                $update = $this->Backhome_Model->photo_delete($data['id'], $data['type']);

                if ($update) {
                    $response = [
                        'status' => true,
                        'content' => 'Başarılı!',
                        'redirect' => site_url("backhome/{$data['type']}/edit/") . $data['id']
                    ];
                } else {
                    $response = [
                        'status' => false,
                        'content' => 'Başarısız!',
                        'redirect' => ''
                    ];
                }

            }
        }

        $this->response_send($response);

    }

}
