<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->library("Advanced_login");

        if ($this->advanced_login->get('logged_in') == true) {

            if ($this->advanced_login->get('is_admin') == true)
                redirect(site_url("/backhome/index"),301);
            else
                redirect(site_url("/index"),301,'');

        }

    }

    public function login($data = [])
    {

        $data = $this->template_load();

//        p($data);
        $this->load->view("partner/login", $data);

    }


    public function template_load($data = [])
    {

        $this->load->library("partner");

        $data["csrf"] = [
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        ];

        $data['head'] = $this->partner->head();
        $data['js'] = $this->partner->js();
        $data["reCaptchaSiteKey"] = $this->config->item("google_data_sitekey");
        $data["registerUrl"] = $this->uri->segment(1);

        return $data;

    }

}
