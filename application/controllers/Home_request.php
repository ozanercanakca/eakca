<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_request extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $response = [];
        $this->load->library("mailoperation");

    }

    public function contact_create($response = [])
    {

        if ($this->input->is_ajax_request() && $this->security->get_csrf_hash() == $this->input->post('csrf_token_oea')) {

            if ($this->input->post('g-recaptcha-response') !== "") {

                $recaptha = $this->authBotCheck($this->input->post('g-recaptcha-response'));

                if ($recaptha->success == true) {

                    $data = [];

                    $set_rules = [
                        [
                            'field' => 'name',
                            'label' => 'Ad',
                            'rules' => 'trim|required|strip_tags|min_length[3]|max_length[50]'
                        ],
                        [
                            'field' => 'email',
                            'label' => 'E-Mail',
                            'rules' => 'trim|required|strip_tags|min_length[5]|max_length[50]|valid_email'
                        ],
                        [
                            'field' => 'phone',
                            'label' => 'Telefon',
                            'rules' => 'trim|required|strip_tags'
                        ],
                        [
                            'field' => 'subject',
                            'label' => 'Konu ',
                            'rules' => 'trim|required|strip_tags'
                        ],
                        [
                            'field' => 'message',
                            'label' => 'Mesaj ',
                            'rules' => 'trim|required|strip_tags'
                        ]
                    ];

                    $this->form_validation->set_rules($set_rules);

                    if ($this->form_validation->run() == FALSE) {

                        $response = [
                            'status' => false,
                            'content' => validation_errors(),
                            'redirect' => ''
                        ];

                    } else {

                        $data = $this->security->xss_clean($this->input->post(),true);

                        unset($data['csrf_token_oea']);
                        unset($data['g-recaptcha-response']);

                        if($data['subject'] == '1'){
                            $subject = 'İletişim';
                        }elseif($data['subject'] == '2'){
                            $subject = 'Proje Teklifi';
                        }elseif($data['subject'] == '3'){
                            $subject = 'Danışmanlık';
                        }elseif($data['subject'] == '4'){
                            $subject = 'Diğer';
                        }else{
                            $subject = ' Belirsiz';
                        }

                        $message_content = "Ad-Soyad: " .   $data['name'] . '-' .
                                                            $data['email'] . '<br/>' .
                                                            $data['phone'] . '<br/>' .
                                                            $subject . '<br/>' .
                                                            $data['message'] . "<br/>" .
                                                            date("Y-m-d H:i:s") . '---' .
                                                            $this->input->ip_address() . '---' .
                                                            $this->input->user_agent();

                        $mailData = [
                            "from" => 'noreply@ercanakca.com',
                            "fromName" => 'ercanakca.com - ' . $data['name'] ,
                            "to" => "hello@ercanakca.com",
                            "cc" => "ozanercanakca@gmail.com",
                            "subject" => 'Yeni İletişim Talebi',
                            "message" => $message_content
                        ];

                        $result = $this->mailoperation->contact_mail_send($mailData);

                        if ($result == "ok") {
                            $response = [
                                'status' => true,
                                'content' => "Mail bize ulaştı.En yakın zamanda sizinle iletişim kuracağız. İyi günler.",
                                'redirect' => "index"
                            ];
                        } else {
                            $response = [
                                'status' => false,
                                'content' => "İşleminiz kaydedilemedi. Lütfen daha sonra tekrar deneyiniz.",
                                'redirect' => ""
                            ];
                        }

                    }

                }

            } else {

                $response = [
                    'status' => false,
                    'content' => 'Lütfen robot olmadığınızı onaylayınız!',
                    'redirect' => ''
                ];

            }

        } else {
            $response = ['status' => false, 'content' => ''];
        }

        $this->response_send($response);

    }

    public function response_send($response = [])
    {

        if (isset($response))
            echo json_encode($response);
        else
            echo json_encode(['status' => false, 'content' => '']);

    }

    public function authBotCheck($captcha = "")
    {

        if ($captcha) {
            $kontrol = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $this->config->item('google_data_secretkey') . "&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']);
            $object = (object)$kontrol;
            $array = (array)$object->scalar;
            $json = json_decode($array[0]);
        } else {
            $json = "false";

        }
        return $json;

    }

}
