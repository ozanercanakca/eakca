<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authreq extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('Userauthreq_Model');
        $response = [];


    }

    public function response_send($response = [])
    {

        if (isset($response))
            echo json_encode($response);
        else
            echo json_encode(['status' => false]);

    }

    public function login()
    {

        if ($this->input->is_ajax_request() && $this->security->get_csrf_hash() == $this->input->post('csrf_token_oea')) {

            if ($this->input->post('g-recaptcha-response') !== "") {

                $recaptha = $this->authBotCheck($this->input->post('g-recaptcha-response'));

                if ($recaptha->success === true) {

                    $data = [];

                    $data = $this->security->xss_clean($this->input->post(), true);

                    $set_rules = [
                        [
                            'field' => 'email',
                            'label' => 'E-Mail',
                            'rules' => 'trim|valid_email|required|strip_tags|min_length[5]|max_length[30]'
                        ],
                        [
                            'field' => 'password',
                            'label' => 'Parola',
                            'rules' => 'trim|required|strip_tags|min_length[5]|max_length[30]'
                        ]
                    ];

                    $this->form_validation->set_rules($set_rules);

                    if ($this->form_validation->run() == FALSE) {

                        $response = [
                            'status' => false,
                            'content' => validation_errors(),
                            'redirect' => ''
                        ];

                    } else {

                        $data['password'] = $this->advanced_login->secure_password($data['password']);

                        $get_user = $this->advanced_login->user_detail($data);

                        if ($get_user != false) {

                            if ($get_user["is_admin"] == 1) {
                                $redirect = site_url('backhome/index');
                            } else {
                                $redirect = site_url('index');
                            }

                            $new_session_data = [
                                'logged_in' => true,
                                'uid' => $get_user["id"],
                                'email' => $get_user['email'],
                                'firstName' => $get_user['firstName'],
                                'lastName' => $get_user['lastName'],
                                'is_admin' => $get_user['is_admin'],
                                'last_ip' => $get_user['last_ip']
                            ];

                            $this->advanced_login->set($new_session_data);

                            $this->log_operation->log_create(
                                [
                                    'uid' => $get_user["id"],
                                    'ip' => $this->input->ip_address(),
                                    'content' => $this->input->user_agent(),
                                    'otype' => 'login',
                                    'utype' => '1',
                                    'status' => 'success'
                                ]
                            );

                            $response = [
                                'status' => true,
                                'content' => 'Başarılı!',
                                'redirect' => $redirect
                            ];

                        } else {

                            $user_detail = $this->advanced_login->user_login_email_detail($this->input->post('email'));

                            if ($user_detail) {

                                $this->log_operation->log_create(
                                    [
                                        'uid' => $user_detail['id'],
                                        'ip' => $this->input->ip_address(),
                                        'content' => $this->input->user_agent(),
                                        'otype' => 'login',
                                        'utype' => '2',
                                        'status' => 'danger'
                                    ]
                                );

                            }

                            $response = [
                                'status' => false,
                                'content' => 'false!',
                                'redirect' => ''
                            ];

                        }

                    }

                } else {

                    $response = [
                        'status' => false,
                        'content' => 'Doğrulama kodu eşleşmiyor!',
                        'redirect' => ''
                    ];

                }

            } else {
                $response = [
                    'status' => false,
                    'content' => 'Doğrulama kodu?',
                    'redirect' => ''
                ];
            }

        } else {
            $response = ["status" => false, "content" => "get out!"];
        }

        $this->response_send($response);

    }

    public function logout()
    {

        /*$this->log_operation->log_create(
            [
                'uid' => $this->session->userdata('uid'),
                'ip' => $this->input->ip_address(),
                'content' => $this->input->user_agent(),
                'otype' => 'logout',
                'utype' => $this->session->userdata('is_admin'),
                'status' => 'success'
            ]
        );*/

        $this->advanced_login->destroy();

        redirect(site_url());

    }

    public function authBotCheck($captcha = "")
    {

        if ($captcha) {
            $kontrol = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $this->config->item('google_data_secretkey') . "&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']);
            $object = (object)$kontrol;
            $array = (array)$object->scalar;
            $json = json_decode($array[0]);
        } else {
            $json = "false";

        }
        return $json;

    }

}
