<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->helper('cookie');

        $this->load->model("Home_Model");

    }

    public function index($offset = 0)
    {

        $data = $this->template_load();

        $limit = 5;

        $result = $this->Home_Model->get_content($limit, $offset);

        $data['blog_list'] = $result['rows'];

        $data['num_results'] = $result['num_rows'];

        $data['pagination'] = $this->pagination($result['rows'], $result['num_rows'], site_url('page'), $limit, 2);

        if ($data['blog_list'] == null) {
            redirect(site_url('error'));
        }

        $this->load->view('frontend/index', $data);

    }

    public function detail($url = '')
    {

        $data = $this->template_load();

        $data['detail'] = $this->Home_Model->content_detail($url);

        if ($data['detail'] == null || $data['detail'] == false) {
            redirect(site_url('error'));
        }

        /*content read count*/

        $cookie_post_id = 'content_read_count_' . $data['detail']->content_id;

        $get_cookie_post = get_cookie("$cookie_post_id", TRUE);

        if ($get_cookie_post === NULL) {

            $cookie = [
                'name' => 'content_read_count_' . $data['detail']->content_id,
                'value' => $data['detail']->id . '_' . $this->input->ip_address(),
                'expire' => '7200',
                'secure' => TRUE
            ];

            set_cookie($cookie);
            //session_destroy();

            $get_count = $this->Home_Model->content_count_detail($url);

            if ($get_count->content_read) {

                if ($get_count->content_read == 0 || $get_count->content_read == "0") {
                    $new_val = "1";
                } else {
                    $new_val = $get_count->content_read + 1;
                }

                $chck = $this->Home_Model->content_count_update(["url_tr" => $url, "content_read" => $new_val]);

            }

        }else{

        }

        $data["read"] = $data['detail']->content_read;


        $data['tags'] = $this->Home_Model->tag_list($data['detail']->content_id);

        $this->load->view('frontend/detail', $data);

    }

    public function whoiam($url = '')
    {

        $data = $this->template_load();


        $data['detail'] = $this->Home_Model->page_detail($url);

        $data['meta'] = $this->Home_Model->page_meta_detail($data['detail']->id);

        if ($url == null || $data['detail'] == false) {
            redirect(site_url('error'));
        }

        $this->load->view('frontend/whoiam', $data);

    }

    public function category($cat = '', $offset = 0)
    {

        $data = $this->template_load();

        $limit = 5;

        $cat = $this->uri->segment(2);

        $offset = ($this->uri->segment(3) != null) ? $this->uri->segment(3) : 0;

        $result = $this->Home_Model->get_category_content($limit, $offset, $cat);

        $data['blog_list'] = $result['rows'];

        $data['num_results'] = $result['num_rows'];
        $data['meta'] = $result['meta'];

        $data['pagination'] = $this->pagination($result['rows'], $result['num_rows'], site_url("category/{$cat}"), $limit, 3);

        if ($data['blog_list'] == false) {
            redirect(site_url('not-found'));
        }

        $this->load->view('frontend/category', $data);

    }

    public function contact()
    {

        $data = $this->template_load();

        $this->load->view('frontend/contact', $data);

    }

    public function error($offset = 0)
    {

        $data = $this->template_load();

        $this->load->view('frontend/error', $data);

    }

    public function not_found($offset = 0)
    {

        $data = $this->template_load();

        $this->load->view('frontend/not_found', $data);

    }

    public function pagination($rows = [], $num_rows = [], $site_url = '', $limit = 0, $url_segment = 0)
    {

        $this->load->library('pagination');
        $config = [];
        $config['base_url'] = $site_url;
        $config['total_rows'] = $num_rows;
        $config['per_page'] = $limit;
        $config['uri_segment'] = $url_segment;
        $config['use_page_numbers'] = true;
        $config['num_links'] = 5;
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);

        $data['pagination'] = $this->pagination->create_links();

        return $data['pagination'];

    }

    public function search($data = [])
    {

        $data = $this->template_load();

        if ($this->input->get('word') !== null && (strlen($this->input->get('word')) >= 3 && strlen($this->input->get('word')) <= 30)
        ) {

            $word = clear_string($this->input->get('word'));

            //$result = $this->Home_Model->get_content_search(2, $offset, $word);
            $result = $this->Home_Model->get_content_search($word);

            $data['blog_list'] = $result['rows'];

            //$data['num_results'] = $result['num_rows'];

            $data['num_results'] = '';

            //$data['pagination'] = $this->pagination($result['rows'], $result['num_rows'], site_url("call/page/$word/"), 2, 3);
            $data['pagination'] = 0;

            if ($data['blog_list'] == null) {
                $error = 'Aradığınız kelime ile ilgili sonuç bulunamadı!';
                $title = '';
            } else {
                $error = '';
                $title = 'Arama Sonuçları: ' . $word;
            }

        } else {

            $data['blog_list'] = [];

            $data['num_results'] = '';

            $data['pagination'] = 0;

            $error = 'Aradığınız kelime yeterli uzunlukta değil!';

            $title = '';

        }

        $data["error"] = $error;
        $data["title"] = $title;


        $this->load->view('frontend/search', $data);

    }

    /*public function all_category()
    {

        $category = $this->Home_Model->get_category();

        foreach ($category as $item) {

            $count = $this->Home_Model->get_category_articles_count($item->id);

            $data[] = (object)['id' => $item->id, 'title_tr' => $item->title_tr, 'title_en' => $item->title_en, 'url_tr' => $item->url_tr, 'url_en' => $item->url_en, 'created_at' => $item->created_at, 'count' => $count];

        }

        return $data;

    }*/

    /*public function popular_list($limit = 0)
    {

        $data = $this->Home_Model->get_popular($limit);

        return $data;

    }*/

    public function template_load($data = [])
    {

        $data['head'] = $this->frontend->head();
        $data['header'] = $this->frontend->header();
        $data['footer'] = $this->frontend->footer();
        $data['js'] = $this->frontend->js();
        $data['menu'] = $this->frontend->menu();
        $data['csrf'] = [
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        ];

        $data['site_key'] = $this->config->item("google_data_sitekey");

        return $data;
    }

}
