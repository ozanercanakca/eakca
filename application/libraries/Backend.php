<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend
{
    private $CI;

    public function __construct()
    {

        $this->CI = &get_instance();
        $this->CI->load->helper('url');

    }

    public function head($data = "")
    {

        $veri = '
            <link rel="icon" href="' . site_url("assets/backend/template2/dist/images/favicon.ico") . '" />
            <!--Plugin CSS-->
            <link href="' . site_url("assets/backend/template2/dist/css/plugins.min.css") . '" rel="stylesheet">
            <!--main Css-->
            <link href="' . site_url("assets/backend/template2/dist/css/main.min.css") . '" rel="stylesheet">
            <link href="' . site_url("assets/backend/template2/assets/css/alertify.css") . '" rel="stylesheet">
            <style>
                .logout_user{display: none!important;}
                @media (max-width: 768px){
                    .logout_user{display: block!important;}
                }
            </style>
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-108018668-1"></script>
            <script>
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag(\'js\', new Date());
                
                gtag(\'config\', \'UA-108018668-1\');
            </script>
        ';

        return $veri;

    }

    public function side_bar($data = "")
    {

        $veri = '<div class="sidebar-scrollarea">
            <ul class="metismenu list-unstyled mb-0" id="menu">
                <li><a href="' . site_url("backhome/index") . '"><i class="fa fa-dashboard pr-1"></i> Dashboard</a></li>
                <li>
                    <a class="has-arrow" href="#" data-toggle="collapse" aria-expanded="false"><i class="icofont pr-1"></i> Page</a>
                    <ul class="collapse list-unstyled">
                        <li><a href="' . site_url("backhome/page/list") . '">List</a></li>
                    </ul>
                </li>
                
                <li>
                    <a class="has-arrow" href="#" data-toggle="collapse" aria-expanded="false"><i class="icofont pr-1"></i> Content</a>
                    <ul class="collapse list-unstyled">
                        <li><a href="' . site_url("backhome/content/create") . '">Create</a></li>
                        <li><a href="' . site_url("backhome/content/list") . '">List</a></li>
                    </ul>
                </li>
                
                <li>
                    <a class="has-arrow" href="#" data-toggle="collapse" aria-expanded="false"><i class="icofont pr-1"></i> Category</a>
                    <ul class="collapse list-unstyled">
                        <li><a href="' . site_url("backhome/category/create") . '">Create</a></li>
                        <li><a href="' . site_url("backhome/category/list") . '">List</a></li>
                    </ul>
                </li>
               
                <li>
                    <a style="" class="logout_user" href="'.site_url("logout").'" ><i class="icofont pr-1"></i> Oturumu Kapat</a>
                </li>
                </ul>
        </div>
        ';

        return $veri;

    }

    public function js($data = "")
    {

        $veri = '
            <script src="' . site_url("assets/backend/template2/dist/js/plugins.min.js") . '"></script>
            <script src="' . site_url("assets/backend/template2/dist/js/common.js") . '"></script>
            <script src="' . site_url("assets/backend/template2/assets/plugins/tinymce/js/tinymce/tinymce.min.js") . '"></script>
            <script src="' . site_url("assets/backend/template2/assets/plugins/alertify/alertify.js") . '"></script>
        ';
        return $veri;

    }

    public function main_top_bar($veri = "")
    {

        $veri = '
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-md-2 align-self-center my-3 my-lg-0">
                        <h6 class="text-uppercase redial-font-weight-700 redial-light mb-0 pl-2">&nbsp;</h6>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="clearfix d-none d-md-inline">
                            <div class="float-sm-right float-none">
                               &nbsp;
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ';

        return $veri;
    }

    public function breadcrumb($veri = "")
    {
        $veri = '
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="row">
                        <div class="col-12 col-md-12">
                            <div class="card redial-border-light redial-shadow">
                                <div class="card-body">
                                    <h6 class="header-title pl-3 redial-relative">Blank Page</h6>
    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ';

        return $veri;
    }

    public function header_fix($data = "")
    {
        $username = $this->CI->session->userdata('firstName') . ' ' . $this->CI->session->userdata('lastName');

        $veri = '
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-lg-3 col-xl-2 align-self-center">
                    <div class="site-logo">
                        <div style="width:65px; height: 30px">&nbsp;</div>
                    </div>
                    <div class="navbar-header">
                        <button type="button" id="sidebarCollapse" class="navbar-btn bg-transparent float-right">
                            <i class="glyphicon glyphicon-align-left"></i>
                            <span class="navbar-toggler-icon"></span>
                            <span class="navbar-toggler-icon"></span>
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </div>
                </div>
                <div class="col-12 col-lg-3 align-self-center d-none d-lg-inline-block">
                    &nbsp;
                </div>
                <div class="col-12 col-lg-6 col-xl-7 d-none d-lg-inline-block">
                    <nav class="navbar navbar-expand-lg p-0">
                        <ul class="navbar-nav notification ml-auto d-inline-flex">
                            <li class="nav-item dropdown  align-self-center"></li>
                            <li class="nav-item dropdown  align-self-center"></li>
                            <li class="nav-item dropdown user-profile align-self-center">
                                <a  class="nav-link" data-toggle="dropdown" aria-expanded="false">
                                    <span class="float-right pl-3 text-white"><i class="fa fa-angle-down"></i></span>
                                    <div class="media">
                                        <img src="'.site_url("assets/backend/template2/").'dist/images/author.jpg" alt="" class="d-flex mr-3 img-fluid redial-rounded-circle-50" width="45" />
                                        <div class="media-body align-self-center">
                                            <p class="mb-2 text-white  font-weight-bold">'.$username.'</p>
                                            <small class="redial-primary-light font-weight-bold text-white"> Admin </small>
                                        </div>
                                    </div>
                                </a>
                                <ul class="dropdown-menu border-bottom-0 rounded-0 py-0">
                                    <li><a class="dropdown-item py-2" href="'.site_url("logout").'"><i class="fa fa-sign-out pr-2"></i> logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>';

        return $veri;

    }

    public function side_chat($data = "")
    {
        $veri = '
            <a href="#" class="setting text-center redial-bg-primary d-none d-lg-block">
                <h4 class="text-white mb-0"><i class="icofont icofont-gear"></i></h4>
            </a>
            <div class="sidbarchat">
                <ul class="nav nav-tabs border-0 justify-content-lg-center my-3 my-lg-0 flex-column flex-sm-row" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link redial-light border-top-0 border-left-0 border-right-0 active pt-0" id="11-tab" data-toggle="tab" href="#11" role="tab" aria-controls="home" aria-selected="true">Chat</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link redial-light border-top-0 border-left-0 border-right-0 pt-0" id="21-tab" data-toggle="tab" href="#21" role="tab" aria-controls="profile" aria-selected="false">Todo</a>
                    </li>
        
                </ul>
                <div class="tab-content py-4" id="mysideTabContent">
                    <div class="tab-pane fade show active" id="11" role="tabpanel" aria-labelledby="11-tab">
                        <ul class="nav flex-column" role="tablist">
                            <li class="nav-item redial-divider px-3">
                                <a class="nav-link active redial-light" data-toggle="tab" href="#tab1" role="tab" aria-selected="true">
                                    <div class="media d-block d-sm-flex text-center text-sm-left py-3">
                                        <img class="img-fluid d-md-flex mr-sm-3 redial-rounded-circle-50" src="'.site_url("assets/backend/template2/").'dist/images/author2.jpg" alt="">
                                        <div class="media-body align-self-center redial-line-height-1_5 mt-2 mt-sm-0">
                                            <h6 class="mb-1 redial-font-weight-800">Harry Jones</h6>
                                            Managing Partner at MDD
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item redial-divider px-3">
                                <a class="nav-link redial-light" data-toggle="tab" href="#tab2" role="tab" aria-selected="false">
                                    <div class="media d-block d-sm-flex text-center text-sm-left py-3">
                                        <img class="img-fluid d-md-flex mr-sm-3 redial-rounded-circle-50" src="'.site_url("assets/backend/template2/").'dist/images/author3.jpg" alt="">
                                        <div class="media-body align-self-center redial-line-height-1_5 mt-2 mt-sm-0">
                                            <h6 class="mb-1 redial-font-weight-800">Daniel Taylor</h6>
                                            Freelance Web Developer
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item redial-divider px-3">
                                <a class="nav-link redial-light" data-toggle="tab" href="#tab3" role="tab" aria-selected="false">
                                    <div class="media d-block d-sm-flex text-center text-sm-left py-3">
                                        <img class="img-fluid d-md-flex mr-sm-3 redial-rounded-circle-50" src="'.site_url("assets/backend/template2/").'dist/images/author.jpg" alt="">
                                        <div class="media-body align-self-center redial-line-height-1_5 mt-2 mt-sm-0">
                                            <h6 class="mb-1 redial-font-weight-800">Charlotte </h6>
                                            Co-Founder & CEO at Pi
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item redial-divider px-3">
                                <a class="nav-link redial-light" data-toggle="tab" href="#tab4" role="tab" aria-selected="false">
                                    <div class="media d-block d-sm-flex text-center text-sm-left py-3">
                                        <img class="img-fluid d-md-flex mr-sm-3 redial-rounded-circle-50" src="'.site_url("assets/backend/template2/").'dist/images/author7.jpg" alt="">
                                        <div class="media-body align-self-center redial-line-height-1_5 mt-2 mt-sm-0">
                                            <h6 class="mb-1 redial-font-weight-800">Jack Sparrow</h6>
                                            Managing Partner at MDD
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item redial-divider px-3">
                                <a class="nav-link redial-light" data-toggle="tab" href="#tab5" role="tab" aria-selected="false">
                                    <div class="media d-block d-sm-flex text-center text-sm-left py-3">
                                        <img class="img-fluid d-md-flex mr-sm-3 redial-rounded-circle-50" src="'.site_url("assets/backend/template2/").'dist/images/author6.jpg" alt="">
                                        <div class="media-body align-self-center redial-line-height-1_5 mt-2 mt-sm-0">
                                            <h6 class="mb-1 redial-font-weight-800">Bhaumik</h6>
                                            Managing Partner at MDD
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item px-3">
                                <a class="nav-link redial-light" data-toggle="tab" href="#tab6" role="tab" aria-selected="false">
                                    <div class="media d-block d-sm-flex text-center text-sm-left py-3">
                                        <img class="img-fluid d-md-flex mr-sm-3 redial-rounded-circle-50" src="'.site_url("assets/backend/template2/").'dist/images/author8.jpg" alt="">
                                        <div class="media-body align-self-center redial-line-height-1_5 mt-2 mt-sm-0">
                                            <h6 class="mb-1 redial-font-weight-800">Wood Walton</h6>
                                            Managing Partner at MDD
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item px-3">
                                <a class="nav-link redial-light" data-toggle="tab" href="#tab6" role="tab" aria-selected="false">
                                    <div class="media d-block d-sm-flex text-center text-sm-left py-3">
                                        <img class="img-fluid d-md-flex mr-sm-3 redial-rounded-circle-50" src="'.site_url("assets/backend/template2/").'dist/images/author8.jpg" alt="">
                                        <div class="media-body align-self-center redial-line-height-1_5 mt-2 mt-sm-0">
                                            <h6 class="mb-1 redial-font-weight-800">Wood Walton</h6>
                                            Managing Partner at MDD
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item px-3">
                                <a class="nav-link redial-light" data-toggle="tab" href="#tab6" role="tab" aria-selected="false">
                                    <div class="media d-block d-sm-flex text-center text-sm-left py-3">
                                        <img class="img-fluid d-md-flex mr-sm-3 redial-rounded-circle-50" src="'.site_url("assets/backend/template2/").'dist/images/author8.jpg" alt="">
                                        <div class="media-body align-self-center redial-line-height-1_5 mt-2 mt-sm-0">
                                            <h6 class="mb-1 redial-font-weight-800">Wood Walton</h6>
                                            Managing Partner at MDD
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item px-3">
                                <a class="nav-link redial-light" data-toggle="tab" href="#tab6" role="tab" aria-selected="false">
                                    <div class="media d-block d-sm-flex text-center text-sm-left py-3">
                                        <img class="img-fluid d-md-flex mr-sm-3 redial-rounded-circle-50" src="'.site_url("assets/backend/template2/").'dist/images/author8.jpg" alt="">
                                        <div class="media-body align-self-center redial-line-height-1_5 mt-2 mt-sm-0">
                                            <h6 class="mb-1 redial-font-weight-800">Wood Walton</h6>
                                            Managing Partner at MDD
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item px-3">
                                <a class="nav-link redial-light" data-toggle="tab" href="#tab6" role="tab" aria-selected="false">
                                    <div class="media d-block d-sm-flex text-center text-sm-left py-3">
                                        <img class="img-fluid d-md-flex mr-sm-3 redial-rounded-circle-50" src="'.site_url("assets/backend/template2/").'dist/images/author8.jpg" alt="">
                                        <div class="media-body align-self-center redial-line-height-1_5 mt-2 mt-sm-0">
                                            <h6 class="mb-1 redial-font-weight-800">Wood Walton</h6>
                                            Managing Partner at MDD
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
        
                    </div>
                    <div class="tab-pane fade" id="21" role="tabpanel" aria-labelledby="21-tab">
                        <ul class="mb-0 list-unstyled inbox">
        
                            <li class="border border-top-0 border-left-0 border-right-0">
                                <a href="#" class="h6">
                                    <div class="form-group mb-0 p-3">
                                        <input type="checkbox" id="scheckbox12">
                                        <label for="scheckbox12" class="redial-dark redial-font-weight-600">John Smith</label>
                                        <small class=\'float-right text-muted\'><i class="fa fa-paperclip pr-1"></i> Aug 10</small>
                                        <small class="d-block pt-2"><i class="fa fa-star pr-2"></i> No Subject Lorem ipsum dolor sit amet </small>
                                    </div>
                                </a>
                            </li>
                            <li class="border border-top-0 border-left-0 border-right-0">
                                <a href="#" class="h6">
                                    <div class="form-group mb-0 p-3">
                                        <input type="checkbox" id="scheckbox13">
                                        <label for="scheckbox13" class="redial-dark redial-font-weight-600">Lauren Boggs</label>
                                        <small class=\'float-right text-muted\'> Nov 5</small>
                                        <small class="d-block pt-2"><i class="fa fa-star pr-2"></i>Invite Lorem ipsum dolor sit amet</small>
                                    </div>
                                </a>
                            </li>
                            <li class="border border-top-0 border-left-0 border-right-0">
                                <a href="#" class="h6">
                                    <div class="form-group mb-0 p-3">
                                        <input type="checkbox" id="scheckbox14">
                                        <label for="scheckbox14" class="redial-dark redial-font-weight-600">Devid Taylor</label>
                                        <small class=\'float-right text-muted\'><i class="fa fa-paperclip pr-1"></i> Jan 25</small>
                                        <small class="d-block pt-2"><i class="fa fa-star pr-2"></i>Developemnt  Lorem ipsum dolor sit amet</small>
        
                                    </div>
                                </a>
                            </li>
                            <li class="border border-top-0 border-left-0 border-right-0">
                                <a href="#" class="h6">
                                    <div class="form-group mb-0 p-3">
                                        <input type="checkbox" id="sscheckbox12">
                                        <label for="sscheckbox12" class="redial-dark redial-font-weight-600">John Smith</label>
                                        <small class=\'float-right text-muted\'><i class="fa fa-paperclip pr-1"></i> Aug 10</small>
                                        <small class="d-block pt-2"><i class="fa fa-star pr-2"></i> No Subject Lorem ipsum dolor sit amet </small>
                                    </div>
                                </a>
                            </li>
                            <li class="border border-top-0 border-left-0 border-right-0">
                                <a href="#" class="h6">
                                    <div class="form-group mb-0 p-3">
                                        <input type="checkbox" id="sscheckbox13">
                                        <label for="sscheckbox13" class="redial-dark redial-font-weight-600">Lauren Boggs</label>
                                        <small class=\'float-right text-muted\'> Nov 5</small>
                                        <small class="d-block pt-2"><i class="fa fa-star pr-2"></i>Invite Lorem ipsum dolor sit amet</small>
                                    </div>
                                </a>
                            </li>
                            <li class="border border-top-0 border-left-0 border-right-0">
                                <a href="#" class="h6">
                                    <div class="form-group mb-0 p-3">
                                        <input type="checkbox" id="sscheckbox14">
                                        <label for="sscheckbox14" class="redial-dark redial-font-weight-600">Devid Taylor</label>
                                        <small class=\'float-right text-muted\'><i class="fa fa-paperclip pr-1"></i> Jan 25</small>
                                        <small class="d-block pt-2"><i class="fa fa-star pr-2"></i>Developemnt  Lorem ipsum dolor sit amet</small>
        
                                    </div>
                                </a>
                            </li>
                            <li class="border border-top-0 border-left-0 border-right-0">
                                <a href="#" class="h6">
                                    <div class="form-group mb-0 p-3">
                                        <input type="checkbox" id="ccheckbox14">
                                        <label for="ccheckbox14" class="redial-dark redial-font-weight-600">Devid Taylor</label>
                                        <small class=\'float-right text-muted\'><i class="fa fa-paperclip pr-1"></i> Jan 25</small>
                                        <small class="d-block pt-2"><i class="fa fa-star pr-2"></i>Developemnt  Lorem ipsum dolor sit amet</small>
        
                                    </div>
                                </a>
                            </li>
                            <li class="border border-top-0 border-left-0 border-right-0">
                                <a href="#" class="h6">
                                    <div class="form-group mb-0 p-3">
                                        <input type="checkbox" id="vcheckbox14">
                                        <label for="vcheckbox14" class="redial-dark redial-font-weight-600">Devid Taylor</label>
                                        <small class=\'float-right text-muted\'><i class="fa fa-paperclip pr-1"></i> Jan 25</small>
                                        <small class="d-block pt-2"><i class="fa fa-star pr-2"></i>Developemnt  Lorem ipsum dolor sit amet</small>
        
                                    </div>
                                </a>
                            </li>
        
                        </ul>
        
                    </div>
        
                </div>
        
            </div>';

        return $veri;
    }
}
