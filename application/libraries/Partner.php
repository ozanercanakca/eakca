<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partner
{
    private $CI;

    public function __construct()
    {

        $this->CI = &get_instance();
        $this->CI->load->helper('url');

    }

    public function head($data = "")
    {

        $veri = '
            <link rel="icon" href="' . site_url("assets/backend/template2/dist/images/favicon.ico") . '" />
            <!--Plugin CSS-->
            <link href="' . site_url("assets/backend/template2/dist/css/plugins.min.css") . '" rel="stylesheet">
            <!--main Css-->
            <link href="' . site_url("assets/backend/template2/dist/css/main.min.css") . '" rel="stylesheet">
            <link href="' . site_url("assets/backend/template2/assets/css/alertify.css") . '" rel="stylesheet">


        ';

        return $veri;

    }

    public function js($data = "")
    {

        $veri = '
            <script src="' . site_url("assets/backend/template2/dist/js/plugins.min.js") . '"></script>
            <script src="' . site_url("assets/backend/template2/dist/js/common.js") . '"></script>
            <script src="' . site_url("assets/backend/template2/assets/plugins/tinymce/js/tinymce/tinymce.min.js") . '"></script>
            <script src="' . site_url("assets/backend/template2/assets/plugins/alertify/alertify.js") . '"></script>
        ';
        return $veri;

    }

}
