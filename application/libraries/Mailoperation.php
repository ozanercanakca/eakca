<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: ercan
 * Date: 03.11.2017
 * Time: 13:58
 */

class Mailoperation {

    public function __construct(){
        $this->CI = & get_instance();
        $this->CI->load->library('email');

    }

    public function contact_mail_send($array = ""){


        $config['protocol']     = 'sendmail';
        $config['smtp_host']    = 'mail.ercanakca.com';
        $config['smtp_user']    = 'noreply@ercanakca.com';
        $config['smtp_pass']    = 'Hgzt25?3';
        $config['smtp_port']    = '465';
        $config['mailtype']     = 'html';

        $this->CI->email->initialize($config);
        $this->CI->email->from($array['from'], $array['fromName']);
        $this->CI->email->to($array['to']);
        $this->CI->email->cc($array['cc']);
        $this->CI->email->subject("Contact");
        $this->CI->email->message('<table style="border:0; margin:0;max-width: 600px; width: 100%; font-family:\'Verdana\',sans-serif; margin:50px auto; border:0; border-collapse:separate; border-spacing:0;">
                <thead>
                    <tr>
                        <th colspan="2" style="width:100%; background: #fff; padding:20px 0; border-bottom:1px solid #f9f9f9;">
                            <img src="https://ercanakca.com/assets/frontend/template3/images/logo.png" style="width: 160px;float: left;margin-left: 20px;">
                        </th>
                    </tr>
                </thead>
                <tbody style="background: #fff;">
                    <tr>
                        <td colspan="2" class="subject" style="padding:10px 20px; border:0; color:#444; box-shadow: 0 1px 10px rgba(0, 0, 0, 0.16);">
                            <p style="font-size:18px; margin:0; padding:10px 0;">'.$array['to'].'</p>
                        </td>
                    </tr>
                    <tr><td colspan="2" ></td></tr>
                  
                    <tr>
                        <td style="padding:10px 20px; border:0;">
                            <strong style="color:#333; font-size:14px">Message: </strong>
                        </td>
                        <td style="padding:10px 20px; border:0;">
                            <p style="font-size:14px; color: #666; line-height: 21px;">'.$array['message'].'</p>
                        </td>
                    </tr>
                </tbody>
                <tfoot style="background-color:  #eaeaea;">
                    <tr>
                        <td  colspan="2" style=" border:0;padding:15px 0;">
                            <p style="font-size:14px; color: #666; line-height: 21px; color:#666; text-align: center; font-size:13px;">&copy; 2018 ercanakca.com.com All Rights Reserved <br><a href="#"  style="color:#0b5ad2;  color:#333;">info@ercanakca.com</a></p>
                        </td>
                    </tr>
                </tfoot>
            </table>');

        $send = $this->CI->email->send();

        if($send){
            $returnResult = "ok";
        }else{
            $returnResult = $this->CI->email->print_debugger();
        }

        return $returnResult;
    }


}
