<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Advanced_login
{

    function __construct()
    {

        $this->CI =& get_instance();
    }

    function destroy()
    {
        $this->CI->session->sess_destroy();
    }

    function set($array)
    {

        $this->CI->session->set_userdata($array);
    }

    function get($key)
    {

        if ($this->CI->session->userdata($key))
            return $this->CI->session->userdata($key);
        else
            return false;
    }

    function secure_password($password = "")
    {

        return hash('sha512', $password . $this->CI->config->item('encryption_key'));

    }

    function secure_id($password = "", $userid = "")
    {

        return hash('sha512', $password . $userid . $this->CI->config->item('encryption_key'));

    }

    function is_admin()
    {

        if ($this->get('is_admin') == '1')
            return true;
        else
            return false;
    }

    function user_detail($data = [])
    {

        $sql = "SELECT * FROM user WHERE email ='" . $data['email'] . "' && password ='" . $data['password'] . "' ";

        $query = $this->CI->db->query($sql, $data);

        if ($query->num_rows() == 1)
            return $query->row_array();
        else
            return false;

    }

    function user_login_email_detail($email = "")
    {

        $sql = "SELECT * FROM user WHERE email ='" . $email . "'";
        $query = $this->CI->db->query($sql, $email);

        if ($query->num_rows() == 1)
            return $query->row_array();
        else
            return false;

    }

}
