<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: ercanakca
 * Date: 02.03.2018
 * Time: 12:00
 * front-end template library class
 */
class Frontend
{

    public function __construct()
    {

        $this->CI = &get_instance();
        $this->CI->load->helper('url');
        $this->CI->load->model('Home_Model');


    }

    public function head($data = "")
    {

        $veri = '
            <meta name="author" content="Ercan Akça - Software Development Team Lead">
            <meta name="theme-color" content="#9A12B3"/>
            <link rel="icon" type="image/png" href="' . site_url("assets/frontend/template3/images/favicon.png") . '">
            <!-- Styles -->
            <link rel="stylesheet" href="' . site_url("assets/frontend/template3/styles/plugins/bootstrap.min.css") . '">
            <link rel="stylesheet" href="' . site_url("assets/frontend/template3/styles/plugins/font-awesome.min.css") . '">
            <link rel="stylesheet" href="' . site_url("assets/frontend/template3/styles/main.css") . '">
            <link rel="stylesheet" href="' . site_url("assets/frontend/template3/prism.css") . '">
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-108018668-1"></script>
            <script>
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag(\'js\', new Date());
                
                gtag(\'config\', \'UA-108018668-1\');
            </script>
            ';

        return $veri;

    }

    public function menu($data = "")
    {

        $veri = '   
            <div class="closed-wrapper">
                <a href="'.site_url("index").'" class="logo">
                    <img src="'.site_url("assets/frontend/template3/images/logo.png").'" width="40" height="38" alt="ercanakca.com">
                </a>
                <a class="right-menu-close"><i class="fa fa-times"></i></a>
            </div>
            <aside style="margin-bottom: 50px !important; ">';

                    $veri .= $this->search();

                    $veri .= $this->pages();

                    $veri .= $this->categories();

                    $veri .= $this->popular_post();

                    $veri .='</aside>';

        return $veri;

    }

    public function header($data = "")
    {

        $veri = '
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <a href="' . site_url('index') . '" class="logo"><img src="'.site_url("assets/frontend/template3/images/logo.png").'" alt="Anasayfa"></a>
                    </div>
                    <div class="col-lg-9">
                        <nav>
                            <ul>
                                <li>
                                    <div class="menu-toggle">
                                        <div>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
    
        ';

        return $veri;

    }

    public function js($data = "")
    {

        $veri = '
            <!-- scripts -->
            <script src="' . site_url("assets/frontend/template3/scripts/jquery-3.2.1.min.js") . '"></script>
            <script src="' . site_url("assets/frontend/template3/scripts/main.js") . '"></script>
            <script src="' . site_url("assets/frontend/template3/scripts/timeago/dist/timeago.js") . '"></script>
            <script src="' . site_url("assets/frontend/template3/scripts/timeago/dist/timeago.locales.min.js") . '"></script>
            <script src="' . site_url("assets/frontend/template3/scripts/timeago/timeago.render.js") . '"></script>
            <script src="' . site_url("assets/frontend/template3/prism.js") . '"></script>
        ';

        return $veri;

    }

    public function search($data = ""){

        /*$hash = $this->CI->security->get_csrf_hash();
        <input type="hidden" name="crxas" value="'. $hash .'"/>*/

        $veri = '
            <div class="sidebar">
                <form method="get" action="'.site_url("call").'" class="search"> 
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Aramak istediğiniz kelime?" name="word">
                        <button><i class="fa fa-search"></i></button>
                    </div>
                </form>
            </div>
        ';

        return $veri;
    }

    public function popular_post($data = ""){

        $veri = '
            <div class="sidebar">
                <div class="popular-post">
                <h3 class="title">POPÜLER YAZILAR</h3>';

                    $popular_list = $this->CI->Home_Model->get_popular(5);

                    if ($popular_list) {
                        foreach ($popular_list as $popular) {

                            if($popular->image != null){
                                $thumb_image = site_url('/upload/content/'.$popular->image);
                            }else{
                                $thumb_image = site_url('/upload/default/placeholder.jpg');
                            }

                             $veri .= '
                                <div class="item">
                                    <div class="image"><img src=" ' . $thumb_image . ' " alt="'.$popular->title_tr.'"></div>
                                    <a href="'.site_url("detail/$popular->url_tr").'" class="title">' . kisalt($popular->title_tr, 60) . '</a>
                                    <span class="need_to_be_rendered" datetime="'.$popular->created_at.'"></span>
                                </div>';
                        }
                    }

                $veri .='
                </div>   
            </div>
        ';

        return $veri;
    }

    public function pages($data = ""){

        $veri = '
            <div class="sidebar">
                <div class="category-menu">
                <h3 class="title">SAYFALAR</h3>';

                    $get_page_list = $this->CI->db->query("select * from page where status = '1' order by id limit 4")->result();

                        if ($get_page_list) {

                            $veri .= "<ul>";

                            $veri .= '<li><a href="' . site_url("index") . '"> Anasayfa </a></li>';

                            foreach ($get_page_list as $item) {

                                $veri .= '<li><a href="' . site_url("me/$item->url_tr") . '">' . $item->title_tr . '</a></li>';

                            }
                            $veri .= '<li><a href="' . site_url("contact") . '"> İletişim </a></li>';

                            $veri .= "</ul>";

                        }

                $veri .='
                </div>   
            </div>
        ';

        return $veri;
    }

    public function categories($data = ""){

        $veri = '
            <div class="sidebar">
                <div class="category-menu">
                <h3 class="title">KATEGORİLER</h3>';

                $category = $this->CI->Home_Model->get_category();

                $data = [];

                foreach ($category as $item) {

                    $count = $this->CI->Home_Model->get_category_articles_count($item->id);

                    $data[] = (object)['id' => $item->id, 'title_tr' => $item->title_tr, 'title_en' => $item->title_en, 'url_tr' => $item->url_tr, 'url_en' => $item->url_en, 'created_at' => $item->created_at, 'count' => $count];

                }

            if ($data) {

                $veri .= "<ul>";

                foreach ($data as $c) {

                    $veri .= '<li><a href="' . site_url("category/$c->url_tr") . '" data-content="(' . $c->count . ')" title="' . $c->title_tr . '">' . $c->title_tr . '</a></li>';

                }

                $veri .= "</ul>";

            }

            $veri .='
                </div>   
            </div>
        ';

        return $veri;
    }

    public function footer($data = "")
    {

        $veri = '
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <span>ercanakca.com &copy; 2018</span>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="social">
                        <a href="https://github.com/ercanakca" target="_blank"><i class="fa fa-github"></i></a>
                        <a href="https://twitter.com/ozanercanakca" target="_blank"><i class="fa fa-twitter"></i></a>
                        <a href="https://instagram.com/ozanercanakca" target="_blank"><i class="fa fa-instagram"></i></a>
                        <a href="//www.dmca.com/Protection/Status.aspx?ID=6b6d8e73-7eae-4b8f-94ac-910d6bca5308" title="DMCA.com Protection Status" class="dmca-badge"> <img src="//images.dmca.com/Badges/dmca-badge-w100-5x1-11.png?ID=6b6d8e73-7eae-4b8f-94ac-910d6bca5308" alt="DMCA.com Protection Status"></a> <script src="//images.dmca.com/Badges/DMCABadgeHelper.min.js"> </script>                                       </div>
                </div>
            </div>
        </div>
        ';

        return $veri;

    }

}
