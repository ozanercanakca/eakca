<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $detail->meta_title_tr;?></title>
    <meta name="description" content="<?php echo $detail->meta_description_tr;?>">
    <meta name="keywords" content="<?php echo $detail->meta_keywords_tr;?>">
    <?php echo $head; ?>
</head>
<body>
<div class="bg-fade"></div>

<!-- right menu -->
<div class="right-menu">
    <?php echo $menu; ?>
</div>
<!-- right menu //. -->

<!-- header -->
<header>
    <?php echo $header; ?>
</header>
<!-- header //. -->

<main class="detail-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <section class="post">
                    <h2 class="none-text"></h2>
                    <article>
                        <div class="row">
                            <div class="col-md-12">
                                <?php

                                if ($detail->image != null) {
                                    $image = site_url('/upload/content/' . $detail->image);
                                    $image = '<img src="' . $image . '" alt="' . $detail->content_title . '" class="img-responsive"/>';
                                } else {
                                    $image = '';
                                }

                                ?>
                                <div class="info">
                                    <a href="#" class="category"><?php echo $detail->category_name; ?></a>
                                    <span class="need_to_be_rendered" datetime="<?php echo $detail->created_at; ?>"></span>
                                    <span><?php echo $read;?> Görüntülenme</span>
                                </div>
                                <h4><?php echo $detail->content_title; ?></h4>

                                <figure>
                                    <?php echo $image; ?>
                                </figure>

                                <?php
                                $replace_content = str_replace('<img', '<img class="img-responsive"', $detail->content_tr);
                                echo nl2br($replace_content);

                                ?>
                                <div class="tags">

                                    <?php
                                    if($tags){
                                      echo '<span>Etiketler: </span>';
                                      foreach ($tags as $key => $tag) {
                                        echo '<a href="#'.$tag->url_tr.'" title="'.$tag->name_tr.' etiketine sahip yazıları görüntüle!">'.$tag->name_tr.'</a> ';
                                      }
                                    }?>

                                </div>
                            </div>
                        </div>
                    </article>
                </section>
            </div>
        </div>
    </div>
</main>

<footer>
    <?php echo $footer; ?>
</footer>


<!-- scripts -->
<?php echo $js; ?>
</body>
</html>
