<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $meta->title_tr;?></title>
    <meta name="description" content="<?php echo $meta->description_tr;?>">
    <meta name="keywords" content="<?php echo $meta->keywords_tr;?>">
    <?php echo $head; ?>
    <link rel="stylesheet" href="<?php echo site_url()?>/assets/frontend/template3/prism.css">
</head>
<body>
<div class="bg-fade"></div>

<!-- right menu -->
<div class="right-menu">
    <?php echo $menu; ?>
</div>
<!-- right menu //. -->

<!-- header -->
<header>
    <?php echo $header; ?>
</header>
<!-- header //. -->

<main class="detail-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <section class="post">
                    <h2 class="none-text">post</h2>
                    <article>
                        <div class="row">
                            <div class="col-md-12">
                                <?php

                                if ($detail->image != null) {
                                    $image = site_url('/upload/page/' . $detail->image);
                                    $image = '<figure><img src="'.$image.'" alt="'.$detail->title_tr.'" class="img-responsive"/></figure>';
                                } else {
                                    $image = '';
                                }
                                ?>

                                <div class="info">
                                    <a href="#" class="category"><?php echo $detail->title_tr; ?></a>
                                    <span class="need_to_be_rendered" datetime="<?php echo $detail->created_at; ?>"></span>
                                </div>

                                <?php echo $image; ?>

                                <?php echo html_entity_decode($detail->content_tr,ENT_QUOTES, 'UTF-8'); ?>

                            </div>
                        </div>
                    </article>
                </section>
            </div>
        </div>
    </div>
</main>

<footer>
    <?php echo $footer; ?>
</footer>


<!-- scripts -->
<?php echo $js; ?>
<script src="<?php echo site_url()?>/assets/frontend/template3/prism.js"></script>

</body>
</html>
