<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>İletişim - ercanakca.com</title>
    <?php echo $head;?>
    <link href="https://ercanakca.com/assets/frontend/template3/styles/alertify.css" rel="stylesheet">
</head>
<body>
<div class="bg-fade"></div>
<!-- right menu -->
<div class="right-menu">
    <?php echo $menu; ?>
</div>
<!-- right menu //. -->
<!-- header -->
<header>
    <?php echo $header; ?>
</header>
<!-- header //. -->

<main class="detail-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <section class="post">
                    <article>
                        <h1 class="title">İLETİŞİM</h1>
                        <div class="contact">
                            <form id="contact_form" action="" method="post">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="name" placeholder="Ad, Soyad">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="phone" class="form-control" name="phone" placeholder="Telefon">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="email" placeholder="E-Mail" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select name="subject" id="" class="form-control">
                                                <option value="">Konu seçiniz</option>
                                                <option value="1">İletişim</option>
                                                <option value="2">Proje Teklifi </option>
                                                <option value="3">Danışmanlık</option>
                                                <option value="4">Diğer</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea name="message" cols="30" rows="10" class="form-control" placeholder="Mesajınız"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="g-recaptcha" data-sitekey="<?php echo $site_key;?>"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="hidden" name="<?php echo $csrf['name'];?>" value="<?php echo $csrf['hash'];?>" id="csrf_token_oea"/>
                                        <input type="submit" name="submit" id="submit" class="btn btn-color">
                                    </div>
                                </div>
                            </form>
<div class="LI-profile-badge"  data-version="v1" data-size="medium" data-locale="tr_TR" data-type="horizontal" data-theme="light" data-vanity="ercan-akca"><a class="LI-simple-link" href='https://tr.linkedin.com/in/ercan-akca?trk=profile-badge'>Ercan A.</a></div>
<script type="text/javascript" src="https://platform.linkedin.com/badges/js/profile.js" async defer></script>                        </div>
                    </article>
                </section>
            </div>
        </div>
    </div>
</main>

<footer>
    <?php echo $footer; ?>
</footer>

<?php echo $js; ?>

<script src="https://ercanakca.com/assets/frontend/template3/scripts/alertify.js"></script>
<script src="https://www.google.com/recaptcha/api.js"></script>

<script type="text/javascript">

    $("#submit").click(function(e) {
        e.preventDefault();

        var urlPost = "<?php echo site_url("home_request/contact_create");?>";
        $.ajax({
            url: urlPost,
            method: 'POST',
            data: $("#contact_form").serialize(),
            success: function(data) {
                var obj = JSON.parse(data);

                if(obj.status == false){
                    alertify.logPosition("bottom left");
                    alertify.error(obj.content);
                    grecaptcha.reset();
                }else if(obj.status == true){

                    $("#contact_form").trigger('reset');

                    alertify.logPosition("bottom left");
                    alertify.success(obj.content);
                    setTimeout(function(){
                        window.location.assign(obj.redirect);
                    },3000);

                }else{
                    return false;
                }

            },
            error: function() {
                return false;
            }
        });
    });

</script>
<!-- scripts -->
</body>
</html>
