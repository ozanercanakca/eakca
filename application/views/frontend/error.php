<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>HATA!</title>
    <?php echo $head; ?>
</head>
<body>
<div class="bg-fade"></div>

<!-- right menu -->
<div class="right-menu">
    <?php echo $menu; ?>
</div>
<!-- right menu //. -->

<!-- header -->
<header>
    <?php echo $header; ?>
</header>
<!-- header //. -->

<main class="detail-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <section class="post">
                    <h2 class="none-text">post</h2>
                    <article>
                        <h1 class="title">HATA!</h1>
                        <div class="contact"> İÇERİK BULUNAMADI.
                        </div>
                    </article>
                </section>
            </div>
        </div>
    </div>
</main>

<footer>
    <?php echo $footer; ?>
</footer>


<!-- scripts -->
<?php echo $js; ?>
</body>
</html>