<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $meta->title_tr != null ? $meta->title_tr : 'ercanakca.com.com';?></title>
    <meta name="description" content="<?php echo $meta->description_tr != null ?  $meta->description_tr  : 'ercanakca.com';?>">
    <meta name="keywords" content="<?php echo $meta->keywords_tr != null ? $meta->keywords_tr  : 'ercanakca.com';?>">
    <?php echo $head; ?>
</head>
<body>
<div class="bg-fade"></div>

<!-- right menu -->
<div class="right-menu">
    <?php echo $menu; ?>
</div>
<!-- right menu //. -->

<!-- header -->
<header>
    <?php echo $header;?>
</header>
<!-- header //. -->

<main>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <section class="post">
                    <h2 class="none-text">post</h2>

                    <?php

                    if ($blog_list) {

                        foreach ($blog_list as $blog) {

                            if($blog->image != null){
                                $str_replace = str_replace('.jpg','_thumb.jpg', $blog->image);
                                $image_dir = site_url('/upload/content/'.$str_replace);
                                $image = '
                                        <div class="col-md-4 col-sm-4">
                                            <figure>
                                                <img src="' . $image_dir . '" alt="'.$blog->content_title.'" class="img-responsive">
                                            </figure>
                                        </div>
                                        ';
                            }else{
                                $image = '';
                                $image_dir = site_url('/upload/default/placeholder.jpg');
                            }

                            echo '
                                    <article>
                                        <div class="row">
                                            <div class="col-md-8 col-sm-8">
                                                <div class="info">
                                                    <a href="' . site_url("category/$blog->category_url") . '" class="category">' . kisalt($blog->category_name, 60) . '</a>
                                                    <span class="need_to_be_rendered" datetime="'.$blog->created_at.'"></span>
                                                </div>
                                                <h4><a href="'.site_url("detail/$blog->content_url").'">' . kisalt($blog->content_title, 60) . '</a></h4>
                                                <p>' . kisalt($blog->summary_tr, 140) . '</p>
                                                <a href="'.site_url("detail/$blog->content_url").'" class="post-view"> Devamını Oku <i class="fa fa-angle-right"></i></a>
                                            </div>
                                            '.$image.'
                                        </div>
                                    </article>
                                ';
                        }

                    }

                    ?>

                    <nav aria-label="Page navigation example">
                        <?php if (strlen($pagination)) echo $pagination; ?>
                    </nav>

                </section>
            </div>
        </div>
    </div>
</main>

<footer>
    <?php echo $footer; ?>
</footer>

<?php echo $js; ?>
</body>
</html>
