<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo isset($title) ? $title : "" ;?></title>
    <?php echo $head; ?>
</head>
<body>
<div class="bg-fade"></div>

<!-- right menu -->
<div class="right-menu">
    <?php echo $menu; ?>
</div>
<!-- right menu //. -->

<!-- header -->
<header>
    <?php echo $header;?>
</header>
<!-- header //. -->

<main>
    <div class="container">
        <div class="row">
            <?php  if(!$error) { ?>
            <div class="col-lg-12">
                <section class="post">
                    <h2 class="none-text">post</h2>

                    <?php

                    if ($blog_list) {

                        foreach ($blog_list as $blog) {

                            if ($blog->image != null) {
                                $str_replace = str_replace('.jpg', '_thumb.jpg', $blog->image);
                                $image_dir = site_url('/upload/content/' . $str_replace);
                                $image = '
                                        <div class="col-md-4 col-sm-4">
                                            <figure>
                                                <img src="' . $image_dir . '" alt="' . $blog->content_title . '" class="img-responsive">
                                            </figure>
                                        </div>
                                        ';
                                $class1 = 'col-md-8 col-sm-8';
                            } else {
                                $image = '';
                                $image_dir = site_url('/upload/default/placeholder.jpg');
                                $class1 = 'col-md-12 col-sm-12';
                            }

                            echo '
                                    <article>
                                        <div class="row">
                                            <div class="' . $class1 . '">
                                                <div class="info">
                                                    <a href="' . site_url("category/$blog->category_url") . '" class="category">' . kisalt($blog->category_name, 60) . '</a>
                                                    <span class="need_to_be_rendered" datetime="' . $blog->created_at . '"></span>
                                                </div>
                                                <h4><a href="' . site_url("detail/$blog->content_url") . '">' . kisalt($blog->content_title, 60) . '</a></h4>
                                                <p>' . kisalt($blog->summary_tr, 140) . '</p>
                                                <a href="' . site_url("detail/$blog->content_url") . '" class="post-view"> Read Me <i class="fa fa-angle-right"></i></a>
                                            </div>
                                            ' . $image . '
                                        </div>
                                    </article>
                                ';
                        }

                    }

                    ?>

                </section>
            </div>
            <?php }else{ ?>

                <div class="col-lg-8">
                    <section class="post">
                        <h2 class="none-text">post</h2>
                        <article>
                            <h1 class="title">ERROR!</h1>
                            <div class="contact"> <?php echo $error;?>
                            </div>
                        </article>
                    </section>
                </div>

            <?php }?>
        </div>
    </div>
</main>

<footer>
    <?php echo $footer; ?>
</footer>

<?php echo $js; ?>
</body>
</html>