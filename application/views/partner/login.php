<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> login </title>
    <?php echo $head; ?>
</head>
<body>

<!-- main-content-->
<div class="wrapper">
    <div class="w-100">
        <div class="row d-flex justify-content-center  pt-5 mt-5">
            <div style="max-width:400px; width:90%"> <!-- default col-3 col-sm-4 -->
                <div class="card">
                    <div class="card-body text-center">
                        <h4 class="mb-0 redial-font-weight-400">Oturum Aç</h4>
                    </div>
                    <div class="redial-divider"></div>
                    <div class="card-body py-4 text-center">
                        <img src="<?php echo site_url("assets/frontend/template3/images/logo.png"); ?>" alt="" class="img-fluid mb-4" style=" width: 50%; ">
                        <form id="loginForm" action="" method="post">
                            <div class="form-group">
                                <input type="email" placeholder="e-mail" name="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="password" placeholder="<?php echo $this->lang->line("p13") ?>" name="password" class="form-control">
                            </div>
                            <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" id="csrf_token_odeo"/>
                            <div class="form-group">
                                <div class="g-recaptcha" data-sitekey="<?php echo $reCaptchaSiteKey; ?>"></div>
                            </div>
                            <!-- <div class="form-group text-left">
                                <input type="checkbox" id="checkbox11">
                                <label for="checkbox11">Remember Me</label>
                            </div> -->
                            <input type="submit" class="btn btn-primary btn-md redial-rounded-circle-50 btn-block" name="submit" id="submit" value="Login">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End main-content-->

<!-- jQuery -->
<?php echo $js; ?>
<script src="https://www.google.com/recaptcha/api.js"></script>
</body>

<script type="text/javascript">

    $("#submit").click(function (e) {
        e.preventDefault();

        var urlPost = "<?php echo site_url("authreq/login");?>";
        $.ajax({
            url: urlPost,
            method: 'POST',
            data: $("#loginForm").serialize(),
            success: function (data) {
                var obj = JSON.parse(data);

                if (obj.status == false) {
                    alertify.logPosition("top right");
                    alertify.error(obj.content);
                    grecaptcha.reset();
                } else if (obj.status == true) {
                    alertify.logPosition("top right");
                    alertify.success(obj.content);
                    setTimeout(function () {
                        window.location.assign(obj.redirect);
                    }, 2000);
                } else {
                    return false;
                }
            },
            error: function () {
                return false;
            }
        });
    });

</script>
</html>
