<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Page List</title>
    <?php echo $head; ?>
</head>
<body>
<!-- header-->
<div id="header-fix" class="header py-4 py-lg-2 fixed-top">
    <?php echo $header_fix; ?>
</div>
<!-- End header-->

<!-- Main-content Top bar-->
<div class="redial-relative mt-80">
    <?php echo $main_top_bar; ?>
</div>
<!-- End Main-content Top bar-->

<!-- main-content-->
<div class="wrapper">
    <nav id="sidebar" class="card redial-border-light px-2 mb-4">
        <?php echo $side_bar; ?>
    </nav>
    <div id="content">
        <div class="row">
            <div class="col-12 col-sm-12">
                <div class="row mb-4">
                    <div class="col-12 col-md-12">
                        <div class="card redial-border-light redial-shadow mb-4">
                            <div class="card-body">
                                <table id="getMainPage" class="table table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Image</th>
                                        <th>Title TR</th>
                                        <th>Title EN</th>
                                        <th>Created At</th>
                                        <th>Status</th>
                                        <th>Transaction</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End main-content-->

<!-- Top To Bottom-->
<a href="#" class="scrollup text-center redial-bg-primary redial-rounded-circle-50">
    <h4 class="text-white mb-0"><i class="icofont icofont-long-arrow-up"></i></h4>
</a>
<!-- End Top To Bottom-->

<!-- Chat-->
<div id="sidechat">

</div>
<!-- End Chat-->

<!-- jQuery -->
<?php echo $js; ?>

<div class="modal fade" id="meta_modal" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="largemodel" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content redial-border-light">
            <div class="modal-header redial-border-light">
                <h5 class="modal-title pt-2" id="exampleModalLabel">Meta Detail</h5>
            </div>

            <div class="redial-shadow">
                <div class="card-body">
                    <div class="custom-tabs2">
                        <div class="response_alert alert-dismissible fade " role="alert">
                            <span class="response_content"></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                    <div class="custom-tabs2">
                        <ul class="nav nav-tabs flex-column flex-md-row" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link redial-light rounded-0 active" data-toggle="tab" href="#tab1" role="tab" aria-selected="true" aria-expanded="true">TR</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link redial-light rounded-0" data-toggle="tab" href="#tab2" role="tab" aria-selected="false" aria-expanded="false">EN</a>
                            </li>
                        </ul>
                        <div class="tab-content border redial-border-light" id="myTabContent">
                            <div class="tab-pane fade active show" id="tab1" role="tabpanel" aria-expanded="true">
                                <div class="card-body">

                                    <div class="form-group">
                                        <label class="redial-font-weight-600">Title (60 Character)</label>
                                        <input type="text" id="title_tr" class="form-control" placeholder="Enter"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="redial-font-weight-600">Description (150 Character)</label>
                                        <textarea class="form-control" id="description_tr" placeholder="Enter"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="redial-font-weight-600">Keywords (150 Character)</label>
                                        <textarea class="form-control" id="keywords_tr" placeholder="Enter"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab2" role="tabpanel" aria-expanded="false">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label class="redial-font-weight-600">Title (60 Character)</label>
                                        <input type="text" id="title_en" class="form-control" placeholder="Enter"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="redial-font-weight-600">Description (150 Character)</label>
                                        <textarea class="form-control" id="description_en" placeholder="Enter"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="redial-font-weight-600">Keywords (150 Character)</label>
                                        <textarea class="form-control" id="keywords_en" placeholder="Enter"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="id" value="">

            <div class="modal-footer redial-border-light">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="submit_send" type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    function meta_detail(id, type = "page") {

        if (id != null) {

            var url_post = "<?php echo site_url("backhome_request/meta_detail"); ?>";
            var csrf_token_oea = "<?php echo $csrf['hash']; ?>";

            $.ajax({
                url: url_post,
                method: 'POST',
                data: {id: id, type: type, csrf_token_oea: csrf_token_oea},
                success: function (data) {
                    var obj = JSON.parse(data);
                    if (obj.status == false) {
                        alertify.error(obj.content);
                    } else if (obj.status == true) {

                        $("#title_tr").val(obj.content.title_tr);
                        $("#description_tr").val(obj.content.description_tr);
                        $("#keywords_tr").val(obj.content.keywords_tr);
                        $("#title_en").val(obj.content.title_en);
                        $("#description_en").val(obj.content.description_en);
                        $("#keywords_en").val(obj.content.keywords_en);
                        $("#id").val(obj.content.id);

                        $("#meta_modal").modal();

                    }
                },
                error: function () {
                    alert("Error!");
                    return false;
                }
            });
        } else {
            return false;
        }

    }

    function meta_update() {

        var url_post = '<?php echo site_url("backhome_request/meta_update"); ?>';

        var title_tr = $("#title_tr").val();
        var description_tr = $("#description_tr").val();
        var keywords_tr = $("#keywords_tr").val();
        var title_en = $("#title_en").val();
        var description_en = $("#description_en").val();
        var keywords_en = $("#keywords_en").val();
        var id = $("#id").val();

        $.ajax({
            url: url_post,
            method: 'POST',
            data: {
                id:id,
                title_tr:title_tr,
                description_tr:description_tr,
                keywords_tr:keywords_tr,
                title_en:title_en,
                description_en:description_en,
                keywords_en:keywords_en
            },
            success: function (data) {
                var obj = JSON.parse(data);
                if (obj.status == false) {
                    $(".response_alert").addClass("alert alert-danger show");
                    $(".response_content").text(obj.content);
                    setTimeout(function () {
                        $(".response_content").removeClass(obj.content);
                        $(".response_alert").removeClass("alert alert-danger show");
                    }, 2000);
                } else if (obj.status == true) {
                    $(".response_alert").addClass("alert alert-success show");
                    $(".response_content").text(obj.content);
                    setTimeout(function () {
                        $(".response_content").removeClass(obj.content);
                        $(".response_alert").removeClass("alert alert-success show");
                        $("#title_tr").val("");
                        $("#description_tr").val("");
                        $("#keywords_tr").val("");
                        $("#title_en").val("");
                        $("#description_en").val("");
                        $("#keywords_en").val("");
                        $("#id").val("");
                        $("#meta_modal").modal('hide');
                    }, 2000);

                }
            },
            error: function () {
                alert("Error!");
                return false;
            }
        });

    }

    $("#submit_send").click(function (e) {
        e.preventDefault();
        meta_update();
    });

    $(document).ready(function () {

        var urlGetData = '<?php echo site_url("backhome_request/get_page_list")?>';
        $('#getMainPage').DataTable({
            "ajax": urlGetData,
            "order": [[0, 'desc']],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Turkish.json"
            }
        });

    });
</script>
</body>
</html>
