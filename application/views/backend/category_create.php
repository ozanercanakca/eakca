<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Category - Create</title>
    <?php echo $head; ?>
</head>
<body>
<!-- header-->
<div id="header-fix" class="header py-4 py-lg-2 fixed-top">
    <?php echo $header_fix; ?>
</div>
<!-- End header-->

<!-- Main-content Top bar-->
<div class="redial-relative mt-80">
    <?php echo $main_top_bar; ?>
</div>
<!-- End Main-content Top bar-->

<!-- main-content-->
<div class="wrapper">
    <nav id="sidebar" class="card redial-border-light px-2 mb-4">
        <?php echo $side_bar; ?>
    </nav>

    <div id="content">
        <div class="row">
            <div class="col-12 col-sm-12">
                <div class="card redial-border-light redial-shadow mb-4 custom-tabs">
                    <div class="card-body">
                        <ul class="nav nav-tabs flex-column flex-sm-row" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link redial-light active" data-toggle="tab" href="#tab1" role="tab"
                                   aria-selected="true">TR Content</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link redial-light " data-toggle="tab" href="#tab2" role="tab"
                                   aria-selected="false">EN Content</a>
                            </li>
                        </ul>

                        <form id="form_data" method="post">

                            <div class="tab-content py-2" id="myTabContent">
                                <div class="tab-pane fade show active" id="tab1" role="tabpanel">
                                    <div class="row redial-dark">

                                        <div class="col-12 col-sm-12 col-lg-12">
                                            <div class="form-group">
                                                <label class="redial-font-weight-600">Title</label>
                                                <input type="text" name="title_tr" class="form-control" placeholder="Enter"/>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="tab-pane fade " id="tab2" role="tabpanel">
                                    <div class="row redial-dark">


                                        <div class="col-12 col-sm-12 col-lg-12">

                                            <div class="form-group">
                                                <label class="redial-font-weight-600">Title</label>
                                                <input type="text" name="title_en" class="form-control" placeholder="Enter"/>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="redial-font-weight-600">Status</label>
                                    <select class="form-control" name="status">
                                        <option value="1" selected>Active</option>
                                        <option value="0">Passive</option>
                                    </select>
                                </div>

                                <div class="redial-divider my-4"></div>
                                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>"/>
                                <button id="submit_send" class="btn btn-primary btn-xs" >Save</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!--    <div class="col-md-6"></div>-->


</div>
<!-- End main-content-->

<!-- Top To Bottom-->
<a href="#" class="scrollup text-center redial-bg-primary redial-rounded-circle-50">
    <h4 class="text-white mb-0"><i class="icofont icofont-long-arrow-up"></i></h4>
</a>
<!-- End Top To Bottom-->

<!-- jQuery -->
<?php echo $js; ?>

<script>
    $(document).ready(function () {
        tinymce.init({selector: 'textarea', language: 'tr_TR'});
    });

    $("#submit_send").click(function (e) {
        e.preventDefault();
        tinyMCE.triggerSave();

        var url_post = '<?php echo site_url("backhome_request/category_create"); ?>';
        $.ajax({
            url: url_post,
            method: 'POST',
            data: $("#form_data").serialize(),
            success: function (data) {
                var obj = JSON.parse(data);
                if (obj.status == false) {
                    alertify.error(obj.content);
                } else if (obj.status == true) {
                    alertify.success(obj.content);
                    setTimeout(function () {
                        window.location.assign(obj.redirect);
                    }, 2000);
                }
            },
            error: function () {
                alert("Error!");
                return false;
            }
        });
    });
</script>
</body>
</html>
