<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin Panel - ercanakca.com</title>
    <?php echo $head;?>
</head>
<body>
<!-- header-->
<div id="header-fix" class="header py-4 py-lg-2 fixed-top">
    <?php echo $header_fix;?>
</div>
<!-- End header-->

<!-- Main-content Top bar-->
<div class="redial-relative mt-80">
    <?php echo $main_top_bar;?>
</div>
<!-- End Main-content Top bar-->

<!-- main-content-->
<div class="wrapper">
    <nav id="sidebar" class="card redial-border-light px-2 mb-4">
        <?php echo $side_bar;?>
    </nav>
    <div id="content">

    </div>
</div>
<!-- End main-content-->

<!-- Top To Bottom-->
<a href="#" class="scrollup text-center redial-bg-primary redial-rounded-circle-50">
    <h4 class="text-white mb-0"><i class="icofont icofont-long-arrow-up"></i></h4>
</a>
<!-- End Top To Bottom-->

<!-- jQuery -->
<?php echo $js;?>
</body>
</html>
