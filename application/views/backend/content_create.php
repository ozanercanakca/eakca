<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Content - Create</title>
    <?php echo $head; ?>
    <link rel="stylesheet" href="<?php echo site_url("assets/backend/template2/assets/plugins/tag/tagmanager.css")?>">
</head>
<body>
<!-- header-->
<div id="header-fix" class="header py-4 py-lg-2 fixed-top">
    <?php echo $header_fix; ?>
</div>
<!-- End header-->

<!-- Main-content Top bar-->
<div class="redial-relative mt-80">
    <?php echo $main_top_bar; ?>
</div>
<!-- End Main-content Top bar-->

<!-- main-content-->
<div class="wrapper">
    <nav id="sidebar" class="card redial-border-light px-2 mb-4">
        <?php echo $side_bar; ?>
    </nav>

    <div id="content">
        <div class="row">
            <div class="col-12 col-sm-12">
                <div class="card redial-border-light redial-shadow mb-4 custom-tabs">
                    <div class="card-body">
                        <ul class="nav nav-tabs flex-column flex-sm-row" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link redial-light active" data-toggle="tab" href="#tab1" role="tab"
                                   aria-selected="true">TR Content</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link redial-light " data-toggle="tab" href="#tab2" role="tab"
                                   aria-selected="false">EN Content</a>
                            </li>
                        </ul>

                        <form id="form_data" method="post" enctype="multipart/form-data">
                            <div class="tab-content py-2" id="myTabContent">
                                <div class="tab-pane fade show active" id="tab1" role="tabpanel">
                                    <div class="row redial-dark">

                                        <div class="col-12 col-sm-12 col-lg-12">
                                            <div class="form-group">
                                                <label class="redial-font-weight-600">Title</label>
                                                <input type="text" name="title_tr" class="form-control" placeholder="Enter"/>
                                            </div>

                                            <div class="form-group">
                                                <label class="redial-font-weight-600">Summary</label>
                                                <input type="text" name="summary_tr" class="form-control" placeholder="Enter"/>
                                            </div>

                                            <div class="form-group">
                                                <label class="redial-font-weight-600">Content</label>
                                                <textarea class="form-control" name="content_tr" placeholder="Enter"></textarea>
                                            </div>

                                            <div class="form-group">
                                                <label class="redial-font-weight-600">Tags</label><br>
                                                <input type="text" name="tags" class="form-control tm-input"/>
                                                <!-- <button class="btn" id="remove_all_tag">remove all tag</button> -->
                                                <input type="hidden" class="tag_list" name="tag_list"/>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                                <div class="tab-pane fade " id="tab2" role="tabpanel">
                                    <div class="row redial-dark">

                                        <div class="col-12 col-sm-12 col-lg-12">

                                            <div class="form-group">
                                                <label class="redial-font-weight-600">Title</label>
                                                <input type="text" name="title_en" class="form-control" placeholder="Enter"/>
                                            </div>

                                            <div class="form-group">
                                                <label class="redial-font-weight-600">Summary</label>
                                                <input type="text" name="summary_en" class="form-control" placeholder="Enter"/>
                                            </div>

                                            <div class="form-group">
                                                <label class="redial-font-weight-600">Content</label>
                                                <textarea class="form-control" name="content_en" id="content_en"  placeholder="Enter"></textarea>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="redial-font-weight-600">Category</label>
                                    <select class="form-control" name="category_id" id="category_id">
                                        <?php
                                        if ($category) {
                                            echo '<option value="">Select</option>';
                                            foreach ($category as $item) {
                                                echo '<option value="' . $item->id . '">' . $item->title_tr . '</option>';
                                            }
                                        } ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="redial-font-weight-600">Are you shown at home?</label>
                                    <select class="form-control" name="main_show" id="main_show">
                                        <option value="1">Evet</option>
                                        <option value="0" selected>Hayır</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="redial-font-weight-600">Status</label>
                                    <select class="form-control" name="status" id="status">
                                        <option value="1" selected>Active</option>
                                        <option value="2">Passive</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <input type="file" name="file" id="file"/>
                                </div>

                                <div class="redial-divider my-4"></div>
                                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>"/>
                                <button id="submit_send" class="btn btn-primary btn-xs">Save</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- End main-content-->

<!-- Top To Bottom-->
<a href="#" class="scrollup text-center redial-bg-primary redial-rounded-circle-50">
    <h4 class="text-white mb-0"><i class="icofont icofont-long-arrow-up"></i></h4>
</a>
<!-- End Top To Bottom-->
<!-- jQuery -->
<?php echo $js; ?>
<script src="<?php echo site_url("assets/backend/template2/assets/plugins/tag/tagmanager.js")?>"></script>
<!-- <script src="< echo site_url("assets/backend/template2/assets/plugins/tag/typeahead.bundle.js")?>"></script> -->
<script>
    $(document).ready(function () {
      tinymce.init({
          selector: 'textarea',
          height: 500,
          relative_urls: false,
          language: 'tr_TR',
          plugins: 'codesample print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template code codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
          toolbar: 'codesample formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | code',
          image_advtab: true,
          language:'tr_TR',
          codesample_languages: [
              {text: 'HTML/XML', value: 'markup'},
              {text: 'JavaScript', value: 'javascript'},
              {text: 'CSS', value: 'css'},
              {text: 'PHP', value: 'php'},
              {text: 'Ruby', value: 'ruby'},
              {text: 'Python', value: 'python'},
              {text: 'Java', value: 'java'},
              {text: 'C', value: 'c'},
              {text: 'C#', value: 'csharp'},
              {text: 'C++', value: 'cpp'}
          ],
      });
      
      jQuery(".tm-input").tagsManager({
         prefilled: [],
         CapitalizeFirstLetter: false,
         delimiters: [9, 13, 44],
         backspace: [8],
         blinkBGColor_1: '#FFC107',
         blinkBGColor_2: '#28A745',
         hiddenTagListName: 'hiddenTagListA',
         hiddenTagListId: null,
         deleteTagsOnBackspace: true,
         tagsContainer: null,
         tagCloseIcon: '×',
         tagClass: '',
         validator: null,
         onlyTagList: false,
         output:".tag_list",
         replace: true,

     });

      console.log("add : " + $(".tag_list").val());

      jQuery(".tm-input").on('tm:pushed', function(e, tag) {
        //alert(tag + " was pushed!");
        //console.log($(".tag_list").val());
      });

      jQuery(".tm-input").on('tm:spliced', function(e, tag) {
        //alert(tag + " was removed!");
        //console.log("removed : " + $(".tag_list").val());
      });

      jQuery('#remove_all_tag').on('click', function (e) {
        e.preventDefault();
        jQuery(".tm-input").tagsManager('empty');
        //console.log("all removed : " + $(".tag_list").val());
      });

    });


    $("#submit_send").click(function (e) {
        e.preventDefault();
        tinyMCE.triggerSave();

        var url_post = '<?php echo site_url("backhome_request/content_create"); ?>';
        var file_data = $('#file').prop('files')[0];
        var form_data = new FormData();
        form_data.append('title_tr', $("input[name=title_tr]").val());
        form_data.append('title_en', $("input[name=title_en]").val());
        form_data.append('content_tr', document.getElementById("content_tr").value);
        form_data.append('content_en', document.getElementById("content_en").value);
        form_data.append('summary_tr', $("input[name=summary_tr]").val());
        form_data.append('summary_en', $("input[name=summary_en]").val());
        form_data.append('category_id', $("#category_id option:selected").val());
        form_data.append('main_show', $("#main_show option:selected").val());
        form_data.append('status', $("#status option:selected").val());
        form_data.append('tag_list', $("input[name=tag_list]").val());
        form_data.append('csrf_token_oea', $("input[name=csrf_token_oea]").val());
        form_data.append('image', file_data);

        $.ajax({
            url: url_post,
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (data) {
                var obj = JSON.parse(data);
                if (obj.status == false) {
                    alertify.error(obj.content);
                } else if (obj.status == true) {
                    alertify.success(obj.content);
                    setTimeout(function () {
                        window.location.assign(obj.redirect);
                    }, 2000);
                }
            },
            error: function (response) {
                return false;
            }
        });

    });

</script>
</body>
</html>
