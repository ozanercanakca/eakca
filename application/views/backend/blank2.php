<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Redial</title>
    <?php echo $head;?>
</head>
<body>
<!-- header-->
<div id="header-fix" class="header py-4 py-lg-2 fixed-top">
    <?php echo $header_fix;?>
</div>
<!-- End header-->

<!-- Main-content Top bar-->
<div class="redial-relative mt-80">
    <?php echo $main_top_bar;?>
</div>
<!-- End Main-content Top bar-->

<!-- main-content-->
<div class="wrapper">
    <nav id="sidebar" class="card redial-border-light px-2 mb-4">
        <?php echo $side_bar;?>
    </nav>
    <div id="content">
        <?php echo $breadcrumb;?>
    </div>
</div>
<!-- End main-content-->

<!-- Top To Bottom-->
<a href="#" class="scrollup text-center redial-bg-primary redial-rounded-circle-50">
    <h4 class="text-white mb-0"><i class="icofont icofont-long-arrow-up"></i></h4>
</a>
<!-- End Top To Bottom-->

<!-- Chat-->
<div id="sidechat">
    <a href="#" class="setting text-center redial-bg-primary d-none d-lg-block">
        <h4 class="text-white mb-0"><i class="icofont icofont-gear"></i></h4>
    </a>
    <div class="sidbarchat">
        <ul class="nav nav-tabs border-0 justify-content-lg-center my-3 my-lg-0 flex-column flex-sm-row" role="tablist">
            <li class="nav-item">
                <a class="nav-link redial-light border-top-0 border-left-0 border-right-0 active pt-0" id="11-tab" data-toggle="tab" href="#11" role="tab" aria-controls="home" aria-selected="true">Chat</a>
            </li>
            <li class="nav-item">
                <a class="nav-link redial-light border-top-0 border-left-0 border-right-0 pt-0" id="21-tab" data-toggle="tab" href="#21" role="tab" aria-controls="profile" aria-selected="false">Todo</a>
            </li>

        </ul>
        <div class="tab-content py-4" id="mysideTabContent">
            <div class="tab-pane fade show active" id="11" role="tabpanel" aria-labelledby="11-tab">
                <ul class="nav flex-column" role="tablist">
                    <li class="nav-item redial-divider px-3">
                        <a class="nav-link active redial-light" data-toggle="tab" href="#tab1" role="tab" aria-selected="true">
                            <div class="media d-block d-sm-flex text-center text-sm-left py-3">
                                <img class="img-fluid d-md-flex mr-sm-3 redial-rounded-circle-50" src="dist/images/author2.jpg" alt="">
                                <div class="media-body align-self-center redial-line-height-1_5 mt-2 mt-sm-0">
                                    <h6 class="mb-1 redial-font-weight-800">Harry Jones</h6>
                                    Managing Partner at MDD
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item redial-divider px-3">
                        <a class="nav-link redial-light" data-toggle="tab" href="#tab2" role="tab" aria-selected="false">
                            <div class="media d-block d-sm-flex text-center text-sm-left py-3">
                                <img class="img-fluid d-md-flex mr-sm-3 redial-rounded-circle-50" src="dist/images/author3.jpg" alt="">
                                <div class="media-body align-self-center redial-line-height-1_5 mt-2 mt-sm-0">
                                    <h6 class="mb-1 redial-font-weight-800">Daniel Taylor</h6>
                                    Freelance Web Developer
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item redial-divider px-3">
                        <a class="nav-link redial-light" data-toggle="tab" href="#tab3" role="tab" aria-selected="false">
                            <div class="media d-block d-sm-flex text-center text-sm-left py-3">
                                <img class="img-fluid d-md-flex mr-sm-3 redial-rounded-circle-50" src="dist/images/author.jpg" alt="">
                                <div class="media-body align-self-center redial-line-height-1_5 mt-2 mt-sm-0">
                                    <h6 class="mb-1 redial-font-weight-800">Charlotte </h6>
                                    Co-Founder & CEO at Pi
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item redial-divider px-3">
                        <a class="nav-link redial-light" data-toggle="tab" href="#tab4" role="tab" aria-selected="false">
                            <div class="media d-block d-sm-flex text-center text-sm-left py-3">
                                <img class="img-fluid d-md-flex mr-sm-3 redial-rounded-circle-50" src="dist/images/author7.jpg" alt="">
                                <div class="media-body align-self-center redial-line-height-1_5 mt-2 mt-sm-0">
                                    <h6 class="mb-1 redial-font-weight-800">Jack Sparrow</h6>
                                    Managing Partner at MDD
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item redial-divider px-3">
                        <a class="nav-link redial-light" data-toggle="tab" href="#tab5" role="tab" aria-selected="false">
                            <div class="media d-block d-sm-flex text-center text-sm-left py-3">
                                <img class="img-fluid d-md-flex mr-sm-3 redial-rounded-circle-50" src="dist/images/author6.jpg" alt="">
                                <div class="media-body align-self-center redial-line-height-1_5 mt-2 mt-sm-0">
                                    <h6 class="mb-1 redial-font-weight-800">Bhaumik</h6>
                                    Managing Partner at MDD
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item px-3">
                        <a class="nav-link redial-light" data-toggle="tab" href="#tab6" role="tab" aria-selected="false">
                            <div class="media d-block d-sm-flex text-center text-sm-left py-3">
                                <img class="img-fluid d-md-flex mr-sm-3 redial-rounded-circle-50" src="dist/images/author8.jpg" alt="">
                                <div class="media-body align-self-center redial-line-height-1_5 mt-2 mt-sm-0">
                                    <h6 class="mb-1 redial-font-weight-800">Wood Walton</h6>
                                    Managing Partner at MDD
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item px-3">
                        <a class="nav-link redial-light" data-toggle="tab" href="#tab6" role="tab" aria-selected="false">
                            <div class="media d-block d-sm-flex text-center text-sm-left py-3">
                                <img class="img-fluid d-md-flex mr-sm-3 redial-rounded-circle-50" src="dist/images/author8.jpg" alt="">
                                <div class="media-body align-self-center redial-line-height-1_5 mt-2 mt-sm-0">
                                    <h6 class="mb-1 redial-font-weight-800">Wood Walton</h6>
                                    Managing Partner at MDD
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item px-3">
                        <a class="nav-link redial-light" data-toggle="tab" href="#tab6" role="tab" aria-selected="false">
                            <div class="media d-block d-sm-flex text-center text-sm-left py-3">
                                <img class="img-fluid d-md-flex mr-sm-3 redial-rounded-circle-50" src="dist/images/author8.jpg" alt="">
                                <div class="media-body align-self-center redial-line-height-1_5 mt-2 mt-sm-0">
                                    <h6 class="mb-1 redial-font-weight-800">Wood Walton</h6>
                                    Managing Partner at MDD
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item px-3">
                        <a class="nav-link redial-light" data-toggle="tab" href="#tab6" role="tab" aria-selected="false">
                            <div class="media d-block d-sm-flex text-center text-sm-left py-3">
                                <img class="img-fluid d-md-flex mr-sm-3 redial-rounded-circle-50" src="dist/images/author8.jpg" alt="">
                                <div class="media-body align-self-center redial-line-height-1_5 mt-2 mt-sm-0">
                                    <h6 class="mb-1 redial-font-weight-800">Wood Walton</h6>
                                    Managing Partner at MDD
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item px-3">
                        <a class="nav-link redial-light" data-toggle="tab" href="#tab6" role="tab" aria-selected="false">
                            <div class="media d-block d-sm-flex text-center text-sm-left py-3">
                                <img class="img-fluid d-md-flex mr-sm-3 redial-rounded-circle-50" src="dist/images/author8.jpg" alt="">
                                <div class="media-body align-self-center redial-line-height-1_5 mt-2 mt-sm-0">
                                    <h6 class="mb-1 redial-font-weight-800">Wood Walton</h6>
                                    Managing Partner at MDD
                                </div>
                            </div>
                        </a>
                    </li>
                </ul>

            </div>
            <div class="tab-pane fade" id="21" role="tabpanel" aria-labelledby="21-tab">
                <ul class="mb-0 list-unstyled inbox">

                    <li class="border border-top-0 border-left-0 border-right-0">
                        <a href="#" class="h6">
                            <div class="form-group mb-0 p-3">
                                <input type="checkbox" id="scheckbox12">
                                <label for="scheckbox12" class="redial-dark redial-font-weight-600">John Smith</label>
                                <small class='float-right text-muted'><i class="fa fa-paperclip pr-1"></i> Aug 10</small>
                                <small class="d-block pt-2"><i class="fa fa-star pr-2"></i> No Subject Lorem ipsum dolor sit amet </small>
                            </div>
                        </a>
                    </li>
                    <li class="border border-top-0 border-left-0 border-right-0">
                        <a href="#" class="h6">
                            <div class="form-group mb-0 p-3">
                                <input type="checkbox" id="scheckbox13">
                                <label for="scheckbox13" class="redial-dark redial-font-weight-600">Lauren Boggs</label>
                                <small class='float-right text-muted'> Nov 5</small>
                                <small class="d-block pt-2"><i class="fa fa-star pr-2"></i>Invite Lorem ipsum dolor sit amet</small>
                            </div>
                        </a>
                    </li>
                    <li class="border border-top-0 border-left-0 border-right-0">
                        <a href="#" class="h6">
                            <div class="form-group mb-0 p-3">
                                <input type="checkbox" id="scheckbox14">
                                <label for="scheckbox14" class="redial-dark redial-font-weight-600">Devid Taylor</label>
                                <small class='float-right text-muted'><i class="fa fa-paperclip pr-1"></i> Jan 25</small>
                                <small class="d-block pt-2"><i class="fa fa-star pr-2"></i>Developemnt  Lorem ipsum dolor sit amet</small>

                            </div>
                        </a>
                    </li>
                    <li class="border border-top-0 border-left-0 border-right-0">
                        <a href="#" class="h6">
                            <div class="form-group mb-0 p-3">
                                <input type="checkbox" id="sscheckbox12">
                                <label for="sscheckbox12" class="redial-dark redial-font-weight-600">John Smith</label>
                                <small class='float-right text-muted'><i class="fa fa-paperclip pr-1"></i> Aug 10</small>
                                <small class="d-block pt-2"><i class="fa fa-star pr-2"></i> No Subject Lorem ipsum dolor sit amet </small>
                            </div>
                        </a>
                    </li>
                    <li class="border border-top-0 border-left-0 border-right-0">
                        <a href="#" class="h6">
                            <div class="form-group mb-0 p-3">
                                <input type="checkbox" id="sscheckbox13">
                                <label for="sscheckbox13" class="redial-dark redial-font-weight-600">Lauren Boggs</label>
                                <small class='float-right text-muted'> Nov 5</small>
                                <small class="d-block pt-2"><i class="fa fa-star pr-2"></i>Invite Lorem ipsum dolor sit amet</small>
                            </div>
                        </a>
                    </li>
                    <li class="border border-top-0 border-left-0 border-right-0">
                        <a href="#" class="h6">
                            <div class="form-group mb-0 p-3">
                                <input type="checkbox" id="sscheckbox14">
                                <label for="sscheckbox14" class="redial-dark redial-font-weight-600">Devid Taylor</label>
                                <small class='float-right text-muted'><i class="fa fa-paperclip pr-1"></i> Jan 25</small>
                                <small class="d-block pt-2"><i class="fa fa-star pr-2"></i>Developemnt  Lorem ipsum dolor sit amet</small>

                            </div>
                        </a>
                    </li>
                    <li class="border border-top-0 border-left-0 border-right-0">
                        <a href="#" class="h6">
                            <div class="form-group mb-0 p-3">
                                <input type="checkbox" id="ccheckbox14">
                                <label for="ccheckbox14" class="redial-dark redial-font-weight-600">Devid Taylor</label>
                                <small class='float-right text-muted'><i class="fa fa-paperclip pr-1"></i> Jan 25</small>
                                <small class="d-block pt-2"><i class="fa fa-star pr-2"></i>Developemnt  Lorem ipsum dolor sit amet</small>

                            </div>
                        </a>
                    </li>
                    <li class="border border-top-0 border-left-0 border-right-0">
                        <a href="#" class="h6">
                            <div class="form-group mb-0 p-3">
                                <input type="checkbox" id="vcheckbox14">
                                <label for="vcheckbox14" class="redial-dark redial-font-weight-600">Devid Taylor</label>
                                <small class='float-right text-muted'><i class="fa fa-paperclip pr-1"></i> Jan 25</small>
                                <small class="d-block pt-2"><i class="fa fa-star pr-2"></i>Developemnt  Lorem ipsum dolor sit amet</small>

                            </div>
                        </a>
                    </li>

                </ul>

            </div>

        </div>

    </div>
</div>
<!-- End Chat-->

<!-- jQuery -->
<?php echo $js;?>
</body>
</html>
