<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Content - Edit</title>
    <?php echo $head; ?>
</head>
<body>
<!-- header-->
<div id="header-fix" class="header py-4 py-lg-2 fixed-top">
    <?php echo $header_fix; ?>
</div>
<!-- End header-->

<!-- Main-content Top bar-->
<div class="redial-relative mt-80">
    <?php echo $main_top_bar; ?>
</div>
<!-- End Main-content Top bar-->

<!-- main-content-->
<div class="wrapper">
    <nav id="sidebar" class="card redial-border-light px-2 mb-4">
        <?php echo $side_bar; ?>
    </nav>

    <div id="content">
        <div class="row">
            <div class="col-12 col-sm-12">
                <div class="card redial-border-light redial-shadow mb-4 custom-tabs">
                    <div class="card-body">
                        <ul class="nav nav-tabs flex-column flex-sm-row" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link redial-light active" data-toggle="tab" href="#tab1" role="tab"
                                   aria-selected="true">TR Content</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link redial-light " data-toggle="tab" href="#tab2" role="tab"
                                   aria-selected="false">EN Content</a>
                            </li>
                        </ul>

                        <form id="form_data" method="post" enctype="multipart/form-data">
                            <div class="tab-content py-2" id="myTabContent">
                                <div class="tab-pane fade show active" id="tab1" role="tabpanel">
                                    <div class="row redial-dark">

                                        <div class="col-12 col-sm-12 col-lg-12">
                                            <div class="form-group">
                                                <label class="redial-font-weight-600">Title</label>
                                                <input type="text" name="title_tr" class="form-control" placeholder="Enter" value="<?php echo $content_detail->title_tr;?>"/>
                                            </div>

                                            <div class="form-group">
                                                <label class="redial-font-weight-600">Summary</label>
                                                <input type="text" name="summary_tr" class="form-control" placeholder="Enter" value="<?php echo $content_detail->summary_tr;?>"/>
                                            </div>

                                            <div class="form-group">
                                                <label class="redial-font-weight-600">Content</label>
                                                <textarea class="form-control" name="content_tr" placeholder="Enter"><?php echo $content_detail->content_tr;?></textarea>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                                <div class="tab-pane fade " id="tab2" role="tabpanel">
                                    <div class="row redial-dark">

                                        <div class="col-12 col-sm-12 col-lg-12">

                                            <div class="form-group">
                                                <label class="redial-font-weight-600">Title</label>
                                                <input type="text" name="title_en" class="form-control" placeholder="Enter" value="<?php echo $content_detail->title_en;?>"/>
                                            </div>

                                            <div class="form-group">
                                                <label class="redial-font-weight-600">Summary</label>
                                                <input type="text" name="summary_en" class="form-control" placeholder="Enter" value="<?php echo $content_detail->summary_en;?>"/>
                                            </div>

                                            <div class="form-group">
                                                <label class="redial-font-weight-600">Content</label>
                                                <textarea class="form-control" name="content_en" placeholder="Enter"><?php echo $content_detail->content_en;?></textarea>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="redial-font-weight-600">Category</label>
                                    <select class="form-control editable editable-click" name="category_id" id="category_id">
                                        <?php
                                        if ($category) {
                                            echo '<option value="">Select</option>';
                                            foreach ($category as $item) {
                                                $selected = ($content_detail->category_id == $item->id) ? " selected='selected'" : "";
                                                echo '<option value="' . $item->id . '" '.$selected.'>' . $item->title_tr . '</option>';
                                            }
                                        } ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="redial-font-weight-600">Are you shown at home?</label>
                                    <select class="form-control editable editable-click" name="main_show" id="main_show">
                                        <option value="1" <?php echo ($content_detail->main_show == 1) ? " selected='selected'" : ""; ?>>Yes</option>
                                        <option value="0" <?php echo ($content_detail->main_show == 0) ? " selected='selected'" : ""; ?>>No</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="redial-font-weight-600">Status</label>
                                    <select class="form-control editable editable-click" name="status" id="status">
                                        <option value="1" <?php echo ($content_detail->status == 1) ? " selected='selected'" : ""; ?>>Yes</option>
                                        <option value="2" <?php echo ($content_detail->status == 2) ? " selected='selected'" : ""; ?>>No</option>
                                    </select>
                                </div>

                                <div class="row">
                                    <div class="col-12 col-sm-6 col-md-3 mb-4 mb-sm-0">
                                        <div class="card redial-border-light text-center">
                                            <?php

                                            if($content_detail->image != null){
                                                $image = site_url("upload/content/$content_detail->image");

                                                $image2 = get_headers($image, 1);
                                                $bytes = $image2["Content-Length"];
                                                $mb = $bytes/(1024 * 1024);
                                                $mb = number_format($mb,2) . " MB";

                                                $button = '<button id="photo_delete" style="float:right" class="btn btn-warning btn-xs">Delete Photo</button>';
                                            }else{
                                                $image = site_url('upload/default/placeholder.jpg');
                                                $button = '';
                                                $mb = '0 MB';
                                            }
                                            ?>

                                            <img src="<?php echo $image;?>" alt="" class="img-fluid rounded-top">
                                            <div class="card-body py-2">
                                                <?php echo $mb;?>
                                                <?php echo '   '.$button;?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-3">
                                        <div class="card redial-border-light text-center">

                                            <input type="file" name="file" id="file"/>

                                        </div>
                                    </div>
                                </div>

                                <div class="redial-divider my-4"></div>
                                <input type="hidden" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>"/>
                                <input type="hidden" name="id" value="<?php echo $content_detail->id; ?>"/>
                                <button id="submit_send" class="btn btn-primary btn-xs">Save</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- End main-content-->

<!-- Top To Bottom-->
<a href="#" class="scrollup text-center redial-bg-primary redial-rounded-circle-50">
    <h4 class="text-white mb-0"><i class="icofont icofont-long-arrow-up"></i></h4>
</a>
<!-- End Top To Bottom-->

<!-- jQuery -->
<?php echo $js; ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.7.10/tinymce.min.js"></script>

<script>
    $(document).ready(function () {
      tinymce.init({
          selector: 'textarea',
          height: 500,
          relative_urls: false,
          language: 'tr_TR',
          plugins: 'codesample print preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template code codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
          toolbar: 'codesample formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | code',
          image_advtab: true,
          language:'tr_TR',
          codesample_languages: [
              {text: 'HTML/XML', value: 'markup'},
              {text: 'JavaScript', value: 'javascript'},
              {text: 'CSS', value: 'css'},
              {text: 'PHP', value: 'php'},
              {text: 'Ruby', value: 'ruby'},
              {text: 'Python', value: 'python'},
              {text: 'Java', value: 'java'},
              {text: 'C', value: 'c'},
              {text: 'C#', value: 'csharp'},
              {text: 'C++', value: 'cpp'}
          ],
      });
    });

    $("#submit_send").click(function (e) {
        e.preventDefault();
        tinyMCE.triggerSave();

        var url_post = '<?php echo site_url("backhome_request/content_update"); ?>';
        var file_data = $('#file').prop('files')[0];
        var form_data = new FormData();
        form_data.append('title_tr', $("input[name=title_tr]").val());
        form_data.append('title_en', $("input[name=title_en]").val());
        form_data.append('content_tr', document.getElementById("content_tr").value);
        form_data.append('content_en', document.getElementById("content_en").value);
        form_data.append('summary_tr', $("input[name=summary_tr]").val());
        form_data.append('summary_en', $("input[name=summary_en]").val());
        form_data.append('category_id', $("#category_id option:selected").val());
        form_data.append('main_show', $("#main_show option:selected").val());
        form_data.append('status', $("#status option:selected").val());
        form_data.append('csrf_token_oea', $("input[name=csrf_token_oea]").val());
        form_data.append('id', $("input[name=id]").val());
        form_data.append('image', file_data);

        $.ajax({
            url: url_post,
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (data) {
                var obj = JSON.parse(data);
                if (obj.status == false) {
                    alertify.error(obj.content);
                } else if (obj.status == true) {
                    alertify.success(obj.content);
                    setTimeout(function () {
                        window.location.assign(obj.redirect);
                    }, 2000);
                }
            },
            error: function (response) {
                return false;
            }
        });

    });

    $("#photo_delete").click(function (e) {
        e.preventDefault();

        var url_post = '<?php echo site_url("backhome_request/photo_delete"); ?>';

        var form_data = new FormData();
        form_data.append('id', $("input[name=id]").val());
        form_data.append('type', 'content');

        $.ajax({
            url: url_post,
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (data) {
                var obj = JSON.parse(data);
                if (obj.status == false) {
                    alertify.error(obj.content);
                } else if (obj.status == true) {
                    alertify.success(obj.content);
                    setTimeout(function () {
                        window.location.assign(obj.redirect);
                    }, 2000);
                }
            },
            error: function (response) {
                return false;
            }
        });

    });
</script>
</body>
</html>
