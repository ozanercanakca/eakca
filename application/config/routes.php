<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';
$route['404_override'] = 'home/error';
$route['translate_uri_dashes'] = FALSE;

/*front-end routes*/
$route['category/(:any)'] = 'home/category/$1';
$route['category/(:any)/(:num)'] = 'home/category/$1/$2';

$route['index'] = 'home/index';
$route['page'] = 'home/index/$1';
$route['contact'] = 'home/contact';
$route['not-found'] = 'home/not_found';

$route['page/(:num)'] = 'home/index/$1';

$route['detail/(:any)'] = 'home/detail/$1';

$route['me/(:any)'] = 'home/whoiam/$1';

//$route['backhome/login'] = 'auth/login';
$route['club'] = 'auth/login';
$route['logout'] = 'authreq/logout';

$route['call'] = 'home/search';



/*front-end routes*/

/*Back-end routes*/

$route['backhome/index'] = 'backhome/index';
$route['backhome/content/create'] = 'backhome/content_create';
$route['backhome/content/edit/(:num)'] = 'backhome/content_edit/$1';
$route['backhome/content/delete'] = 'backhome_request/content_delete';
$route['backhome/content/list'] = 'backhome/content_list';
$route['backhome/category/create'] = 'backhome/category_create';
$route['backhome/category/edit/(:num)'] = 'backhome/category_edit/$1';
$route['backhome/category/delete'] = 'backhome_request/category_delete';
$route['backhome/category/list'] = 'backhome/category_list';
$route['backhome/meta/detail'] = 'backhome_request/meta_detail';
$route['backhome/meta/update'] = 'backhome_request/meta_update';
$route['backhome/page/list'] = 'backhome/page_list';
$route['backhome/page/edit/(:num)'] = 'backhome/page_edit/$1';

/*Back-end routes*/
